// This service has function for default options in forms

import { Injectable } from '@angular/core';
import { TwoOptionInterface } from '@interfaces';

@Injectable({
  providedIn: 'root'
})
export class DefaultOptionsService {

  // INITIALIZE OPTIONS FORM STARTS

  // Genders
  genderOptions: TwoOptionInterface[] = [
    { value: 'M', viewValue: 'Masculino' },
    { value: 'F', viewValue: 'Femenino' }
  ];

  get genders(): TwoOptionInterface[] {
    return this.genderOptions;
  }

  constructor() { }
}
