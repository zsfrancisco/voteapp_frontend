// Ths service controls the logic to get my ip address

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IpAddressService {

  constructor(
    private http: HttpClient
  ) { }

  // getIPAddress returns the public ip address
  getIPAddress() {  
    return this.http.get("http://api.ipify.org/?format=json");  
  }  

}
