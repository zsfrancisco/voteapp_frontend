// This service controls the logic for the electoral process

import { Injectable } from '@angular/core';
import { URL_API } from '../common/constants';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { NewProcessFormBodyInterface, ProcessReturned, NewReportNewsFormBodyInterface, ChangeStatusProcessInterface, RegisterVotePetitionInterface, RegisterVoteInterface, EditProcessFormBodyInterface, CandidateRequest } from '@interfaces';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable, interval } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ProcessService {

  private url = URL_API;

  // Petition paths
  private paths: any = {
    voting: 'voting_process',
    uploadImage: 'voter/upload_image',
    userSuitable: 'voter/voter_enabled',
    voterRequest: 'voter_request',
    processImage: 'voting_process/image/',
    changeStatus: 'voting_process/change_status',
    votersLabels: 'voting_process_voters/labels/',
    registerVote: 'electoral_vote',
    winner: 'voting_process/winner/',
    adminWinner: 'voting_process/electoral_result/',
    checkCertificate: 'electoral_vote',
    authenticationLogs: 'authentication_logs/',
    votersProcess: 'voting_process_voters/',
    candidateRequest: 'candidate_request',
  }

  // Token
  private token: string = this.authService.readToken();

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private sanitizer: DomSanitizer,
  ) {
    localStorage.setItem("execute", "10");
  }

  // Returns all of Processes
  getProcesses() {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.voting}`, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            resolve(answer.voting_process as ProcessReturned[]);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // Returns a specific process by Id
  getProcess(processId: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const processes = await this.getProcesses();
        (processes as ProcessReturned[]).forEach(process => {
          if (process.id == processId) resolve(process);
        });
      } catch (error) {
        reject(`No se encuentra el proceso con el ID: ${processId}`);
      }
    });
  }

  getProcessImage(processId: string) {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      console.log(`url que se hace la pet: ${this.url}${this.paths.processImage}${processId}`);
      this.http.get(`${this.url}${this.paths.processImage}${processId}`, { headers: headers, responseType: 'blob' })
        .subscribe((answer: any) => {
          if (answer.code === '404') {
            console.log('entra al reject por 404: ', answer);
            reject(answer);
          } else {
            console.log('entra al resolve: ', answer);
            resolve(this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(answer)));
          }
        }, (error: any) => {
          console.log('entra al reject error: ', error);
          reject(error);
        });
    });
  }

  // Register a new process record in the data base
  newProcess(petition: NewProcessFormBodyInterface) {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // Form-data petition
    const formData = new FormData();
    formData.append('file_users', petition.file_users);
    formData.append('json', JSON.stringify(petition.json));
    formData.append('file0', petition.file0);
    // Promise resolve the petition when status is not 400
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.voting}`, formData, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            // console.log('respuesta nuevo proceso: ', answer);
            resolve(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // edit a record process record in the data base
  editProcess(petition: EditProcessFormBodyInterface, processId: string) {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // Form-data petition
    const formData = new FormData();
    formData.append('file_users', petition.file_users);
    formData.append('json', JSON.stringify(petition.json));
    formData.append('file0', petition.file0);
    // Promise resolve the petition when status is not 400
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.voting}/${processId}?_method=PUT`, formData, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            // console.log('respuesta nuevo proceso: ', answer);
            resolve(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // Change the specific process record status
  changeStatus(petition: ChangeStatusProcessInterface) {
    return new Promise(async (resolve, reject) => {
      try {
        // Petition to build the json in the body
        const body = new HttpParams()
          .set('json', JSON.stringify(petition));
        // HttpOptions petition
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': this.token,
            'Content-Type': 'application/x-www-form-urlencoded'
          })
        };
        console.log('petición: ', petition, `url: ${this.url}${this.paths.changeStatus}`);
        // Promise returns the resolve when the answer code is 200
        this.http.post(`${this.url}${this.paths.changeStatus}`, body.toString(), httpOptions)
          .subscribe((answer: any) => {
            if (answer.code === '200') {
              resolve(answer);
            }
            else {
              // console.log('respuesta cancelar proceso: ', answer);
              reject(answer);
            }
          }, (error: any) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  // Upload a image of a voter in the server
  uploadVoterImage(file: File) {
    // Headers petition
    const headers = { 'Authorization': this.token };
    // FormData
    const formData = new FormData();
    formData.append('file0', file);
    // Promise returns the resolve when the answer code is 200
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.uploadImage}`, formData, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          if (answer.code === '200') {
            // console.log('respuesta nuevo candidato: ', answer);
            resolve(answer);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // consultSuitable consults if an user is suitable for vote in one o more electoral processes
  consultSuitable(userId: string) {
    // Headers petition
    const headers = { 'Authorization': this.token };

    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.userSuitable}/${userId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          if (answer.code === '200') {
            // console.log('respuesta nuevo candidato: ', answer);
            resolve(answer.voters_voting_processes);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // reportNews reports to the admin via wmail the user news
  reportNews(petition: NewReportNewsFormBodyInterface) {
    return new Promise((resolve, reject) => {
      const body = new HttpParams()
        .set('json', JSON.stringify(petition));
      // HttpOptions petition
      const httpOptions = {
        headers: new HttpHeaders({
          'Authorization': this.token,
          'Content-Type': 'application/x-www-form-urlencoded'
        })
      };
      // console.log('petición: ', petition, `url: ${this.url}${this.paths.voting}/${processId}`);
      // Promise returns the resolve when the answer code is 200
      this.http.post(`${this.url}${this.paths.voterRequest}`, body.toString(), httpOptions)
        .subscribe((answer: any) => {
          if (answer.code === '200') {
            resolve(answer);
          }
          else {
            // console.log('respuesta cancelar proceso: ', answer);
            reject(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // reportNews reports to the admin via wmail the user news
  reportCandidateNews(petition: CandidateRequest) {
    return new Promise((resolve, reject) => {
      const body = new HttpParams()
        .set('json', JSON.stringify(petition));
      // HttpOptions petition
      const httpOptions = {
        headers: new HttpHeaders({
          'Authorization': this.token,
          'Content-Type': 'application/x-www-form-urlencoded'
        })
      };
      // console.log('petición: ', petition, `url: ${this.url}${this.paths.voting}/${processId}`);
      // Promise returns the resolve when the answer code is 200
      this.http.post(`${this.url}${this.paths.candidateRequest}`, body.toString(), httpOptions)
        .subscribe((answer: any) => {
          if (answer.code === '200') {
            resolve(answer);
          }
          else {
            // console.log('respuesta cancelar proceso: ', answer);
            reject(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // checkCertificate consults if an user's vote certificate is valid
  checkCertificate(numberCertificate: string) {
    // Headers petition
    const headers = { 'Authorization': this.token };

    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.checkCertificate}/${numberCertificate}`)
        .subscribe((answer: any) => {
          if (answer.code != 200) {
            reject(answer.message);
          }
          if (answer.code === '200') {
            // console.log('respuesta nuevo candidato: ', answer);
            resolve(answer.certificate);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // votersLabels returns the list of the voters that can vote in the process with id = processId
  votersLabels(processId: string) {
    // Headers petition
    // const headers = { 'Authorization': this.token };

    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.votersLabels}${processId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          if (answer.code === '200') {
            console.log('respuesta nuevo candidato: ', answer);
            resolve(answer.voters_labels);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  registerVote(petition: RegisterVoteInterface, token: string) {
    return new Promise(async (resolve, reject) => {
      try {
        // Petition to build the json in the body
        const body = new HttpParams()
          .set('json', JSON.stringify(petition));
        // HttpOptions petition
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': token,
            'Content-Type': 'application/x-www-form-urlencoded'
          })
        };
        console.log('petición: ', petition, `url: ${this.url}${this.paths.registerVote}`);
        // Promise returns the resolve when the answer code is 200
        this.http.post(`${this.url}${this.paths.registerVote}`, body.toString(), httpOptions)
          .subscribe((answer: any) => {
            if (answer.code === '200') {
              resolve(answer);
            }
            else {
              // console.log('respuesta cancelar proceso: ', answer);
              reject(answer);
            }
          }, (error: any) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  viewPublicWinner(processId: string) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.winner}${processId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          if (answer.code === '200') {
            console.log('respuesta viewPublicWinner: ', answer);
            resolve(answer.results_winner);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  viewAdminWinner(processId: string) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.adminWinner}${processId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          if (answer.code === '200') {
            console.log('respuesta viewPublicWinner: ', answer);
            resolve(answer);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  authenticationLogs(processId: string) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.authenticationLogs}${processId}`)
        .subscribe((answer: any) => {
          if (answer.code === '404') {
            reject(answer.message);
          }
          if (answer.code === '200') {
            resolve(answer.authentication_log);
          }
          else reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  getVotersByProcess(processId: string) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.votersProcess}${processId}`)
        .subscribe((answer: any) => {
          answer.code == "200" ? resolve(answer.voters_pdf) : reject("No exite el proceso de votación");
        }, (error) => reject(error));
    })
  }

}