// this service controls the logic for the data of the candidates

import { Injectable } from '@angular/core';
import { NewCandidateFormBodyInterface, CandidateReturnedInterface, EditCandidateFormBody } from '@interfaces';
import { AuthService } from './auth.service';
import { URL_API } from '../common/constants';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  private url = URL_API;

  // Paths petitons
  private paths: any = {
    candidate: 'candidate',
    candidates: 'voting_process_candidates',
    candidateImage: 'candidate/image/',
    candidateProposals: 'electoral_proposals',
  }

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private sanitizer: DomSanitizer,
  ) { }

  // getCandidatesByProcessId returns the candidates from a specific process
  getCandidatesByProcessId(processId: string) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };

    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.candidates}/${processId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            console.log('respuesta candidatos del proceso: ', processId, answer);
            const votingProcessCandidates: CandidateReturnedInterface[] = answer.voting_process_candidates;
            resolve(votingProcessCandidates);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // getCandidatesByProcessId returns the candidates from a specific process
  getReportCandidatesByProcessId(processId: string) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };

    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.candidates}/${processId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            console.log('respuesta candidatos del proceso: ', processId, answer);
            const candidatesUrl = answer.candidates_pdf;
            resolve(candidatesUrl);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // getCandidateById returns a specific candidate by id
  getCandidateById(candidateId: string) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };

    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.candidate}/${ candidateId }`)
        .subscribe((answer: any) => {
          answer.code === '200' ? resolve(answer.candidate as CandidateReturnedInterface) : reject(answer)
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // newCandidate saves a candidate record in a specific process
  newCandidate(petition: NewCandidateFormBodyInterface) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };

    const formData = new FormData();
    formData.append('json', JSON.stringify(petition.json));
    formData.append('file0', petition.file0);

    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.candidate}`, formData, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            console.log('respuesta nuevo candidato: ', answer);
            resolve(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // editCandidate saves a candidate record in a specific process
  editCandidate(petition: EditCandidateFormBody, candidateId: string) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };

    const formData = new FormData();
    formData.append('json', JSON.stringify(petition.json));
    formData.append('file0', petition.file0);

    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.candidate}/${candidateId}?_method=PUT`, formData, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            console.log('respuesta editar candidato: ', answer);
            resolve(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // updateCandidateProposals update the proposals of a candidate
  updateCandidateProposals() {
    // TODO
  }

  // deleteCandidate deletes a specific candidate record in a specific process
  deleteCandidate(candidateId: string) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.url}${this.paths.candidate}/${candidateId}`)
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            console.log('respuesta borrar candidato: ', answer);
            resolve(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // getCandidateImage gets the image of the one candidate
  getCandidateImage(processId: string, candidateId: string) {
    console.log('candidato id: ', candidateId);
    let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      console.log(`url que se hace la pet: ${this.url}${this.paths.candidateImage}`);
      this.http.get(`${this.url}${this.paths.candidateImage}${processId}_${candidateId}`, { headers: headers, responseType: 'blob' })
        .subscribe((answer: any) => {
          if (answer.code === '404') {
            console.log('entra al reject por 404: ', answer);
            reject(answer);
          } else {
            console.log('entra al resolve: ', answer);
            resolve(this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(answer)));
          }
        }, (error: any) => {
          console.log('entra al reject error: ', error);
          reject(error);
        });
    });
  }

}
