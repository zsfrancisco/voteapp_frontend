// This service contains functions to manage alerts

import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor(
    private router: Router
  ) { }

  // This function shows a success alert
  succesAlert(message: string) {
    Swal.fire({
      icon: 'success',
      title: '¡Éxito!',
      text: message,
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false,
      showConfirmButton: false,
      timer: 1500
    });
  }

  // This function shows  an error alert
  errorAlert(message: string) {
    Swal.fire({
      icon: 'error',
      title: '¡Error!',
      text: message,
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false,
      showConfirmButton: true,
      confirmButtonColor: '#05668d',
      confirmButtonText: '<span class="span-alert-error">Aceptar</span>',
    });
  }

  // This function shows  an error alert, then redirect to another page
  errorAlertRedirect(message: string, route: string) {
    Swal.fire({
      icon: 'error',
      title: '¡Error!',
      text: message,
      allowOutsideClick: false,
      allowEscapeKey: false,
      confirmButtonColor: '#05668d',
      confirmButtonText: '<span class="span-alert-error">Aceptar</span>',
    }).then((result) => {
      if (result.value) {
        this.router.navigateByUrl(`/${route}`);
      }
    });
  }

  // This function shows a loading alert
  loadingAlert(message: string) {
    Swal.fire({
      icon: 'info',
      title: 'Espere',
      text: message,
      allowOutsideClick: false,
      allowEscapeKey: false,
      allowEnterKey: false,
    });
    Swal.showLoading();
  }

  // This function closes an alert
  closeAlert() {
    Swal.close();
  }

}
