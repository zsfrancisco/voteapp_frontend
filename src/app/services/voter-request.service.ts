import { Injectable } from '@angular/core';
import { URL_API } from '../common/constants';
import { AuthService } from './auth.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { IVoterRequest, IJsonChangeVoterRequest } from '@interfaces';

@Injectable({
  providedIn: 'root'
})
export class VoterRequestService {

  private url = URL_API;

  private paths: any = {
    request: "voter_request",
    changeRequest: "voter_request/reply",
  }

  // Token
  private token: string = this.authService.readToken();

  constructor(
    private http: HttpClient,
    private authService: AuthService,
  ) { }

  // getVotersRequest Returns all of Voters requests
  getVotersRequest() {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.request}`, { headers })
        .subscribe((answer: any) => {
          answer.code == "200" ? resolve(answer.voterRequest as IVoterRequest[]) : reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // getVotersRequest Returns all of Voters requests
  getVoterRequest(voterRequestId: string) {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.request}/${voterRequestId}`, { headers })
        .subscribe((answer: any) => {
          answer.code == "200" ? resolve(answer.voterRequest as IVoterRequest) : reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  changeVoterRequest(petition: IJsonChangeVoterRequest) {
    return new Promise(async (resolve, reject) => {
      try {
        const body = new HttpParams()
          .set('json', JSON.stringify(petition));
        // HttpOptions petition
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': this.token,
            'Content-Type': 'application/x-www-form-urlencoded'
          })
        };
        console.log('petición: ', petition, `url: ${this.url}${this.paths.changeRequest}`);
        // Promise returns the resolve when the answer code is 200
        this.http.post(`${this.url}${this.paths.changeRequest}`, body.toString(), httpOptions)
          .subscribe((answer: any) => {
            if (answer.code == "200") {
              resolve("La solicitud se actualizó correctamente");
            }
            else{
              reject(answer.message);
            }
          }, (error: any) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

}
