// This service controls the access to the system

import { Injectable } from '@angular/core';
import { URL_API } from '../common/constants';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserLoginPetitionInterface, UserLoginReturnedInterface } from '@interfaces';
import { EncryptionService } from './encryption.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // General url
  private url = URL_API;

  // User data
  userToken: string;
  userRole: string;
  userId: string;

  // Petition paths
  private paths: any = {
    userLogin: 'user/login',
    userById: 'user/',
  }

  constructor(
    private http: HttpClient,
    private encryptionService: EncryptionService
  ) { }

  // This function saves the user data in the localStorage
  saveSession(token: string, userId: string, roleId: string) {
    this.userToken = this.encryptionService.encryptData(token);
    this.userId = this.encryptionService.encryptData(userId);
    this.userRole = this.encryptionService.encryptData(roleId);
    localStorage.setItem('token', this.userToken);
    localStorage.setItem('user', this.userId);
    localStorage.setItem('role', this.userRole);
  }

  // Read functions: These functions get the specific user data: token, userId or RoleId
  readToken() {
    localStorage.getItem('token') ? this.userToken = this.encryptionService.decryptData(localStorage.getItem('token')) : this.userToken = '';
    return this.userToken;
  }
  readUserId() {
    localStorage.getItem('user') ? this.userId = this.encryptionService.decryptData(localStorage.getItem('user')) : this.userId = '';
    return this.userId;
  }
  readRoleId() {
    localStorage.getItem('role') ? this.userRole = this.encryptionService.decryptData(localStorage.getItem('role')) : this.userRole = '';
    return this.userRole;
  }

  // isAuth: returns true is user is logged
  isAuth(): boolean {
    let token = this.readToken();
    if (token.length < 2) {
      return false;
    }
    return true;
  }

  // login for the user
  loginUser(petition: UserLoginPetitionInterface) {
    console.log("Petición: ", petition);
    let params = new HttpParams()
      .set('json', JSON.stringify(petition));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.userLogin}`, params, httpOptions)
        .subscribe((data: any) => {
          data.error ? reject(data as UserLoginReturnedInterface) : resolve(data as UserLoginReturnedInterface);
        }, (error: any) => {
          reject(error);
        });
    });

  }

  // this function removes the items of the localStorage
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('role');
  }

  // accessVote creates a variable "voting" in the localStorage
  accessVote(processId: string, voterId: string) {
    const password: string = `NubTWq'ma]>g(W]=?pE-ak;T{6Pj,G&n&7eB'3h@g&Nt;X/Kb$B<b}A$u{L~X{x}5mze,Z`;
    const passwordEncrypted = this.encryptionService.encryptData(password);
    localStorage.setItem('voting', passwordEncrypted);
    const currentProcess = this.encryptionService.encryptData(processId);
    localStorage.setItem('currentProcess', currentProcess);
    const currentVoter = this.encryptionService.encryptData(voterId);
    localStorage.setItem('currentVoter', currentVoter);
  }

  // readVoting returns the value of variable voting in localstorage
  readVoting() {
    const password: string = localStorage.getItem('voting') ? this.encryptionService.decryptData(localStorage.getItem('voting')) : '';
    return password;
  }
  
  readCurrentProcess() {
    const currentProcess: string = localStorage.getItem('currentProcess') ? this.encryptionService.decryptData(localStorage.getItem('currentProcess')) : '';
    return currentProcess;
  }

  readcurrentVoter() {
    const currentVoter: string = localStorage.getItem('currentVoter') ? this.encryptionService.decryptData(localStorage.getItem('currentVoter')) : '';
    return currentVoter;
  }

  isVoter(): boolean {
    const password: string = this.readVoting();
    return password ===  `NubTWq'ma]>g(W]=?pE-ak;T{6Pj,G&n&7eB'3h@g&Nt;X/Kb$B<b}A$u{L~X{x}5mze,Z`;
  }

  closeVoterSession() {
    localStorage.removeItem('voting');
    localStorage.removeItem('currentProcess');
    localStorage.removeItem('currentVoter');
  }

  getUserById(userId: string) {
    return new Promise ((resolve, reject) => {
      this.http.get(`${ this.url }${ this.paths.userById }${ userId }`)
        .subscribe((answer: any) => {
          answer.code == "200" ? resolve(answer.user) : reject("Error la información del usuario no existe");
        }, (error) => reject(error));
    });
  }

}