import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CandidateRequestReturned, JsonChangeCandidateRequest } from '@interfaces';
import { URL_API } from '../common/constants';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CandidateRequestService {

  private url = URL_API;

  private paths: any = {
    request: "candidate_request",
    changeRequest: "candidate_request/reply",
  }

  // Token
  private token: string = this.authService.readToken();

  constructor(
    private http: HttpClient,
    private authService: AuthService,
  ) { }

  // getCandidatesRequest Returns all of candidates requests
  getCandidatesRequest() {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.request}`, { headers })
        .subscribe((answer: any) => {
          answer.code == "200" ? resolve(answer.candidateRequest as CandidateRequestReturned[]) : reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // getCandidateRequest Returns a candidate request by Id
  getCandidateRequest(candidateRequestId: string) {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.request}/${candidateRequestId}`, { headers })
        .subscribe((answer: any) => {
          console.log("Answer: ",answer);
          answer.code == "200" ? resolve(answer.candidateRequest as CandidateRequestReturned) : reject(answer);
        }, (error: any) => {
          reject(error);
        });
    });
  }

  changeCandidateRequest(petition: JsonChangeCandidateRequest) {
    return new Promise(async (resolve, reject) => {
      try {
        const body = new HttpParams()
          .set('json', JSON.stringify(petition));
        // HttpOptions petition
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': this.token,
            'Content-Type': 'application/x-www-form-urlencoded'
          })
        };
        console.log('petición: ', petition, `url: ${this.url}${this.paths.changeRequest}`);
        // Promise returns the resolve when the answer code is 200
        this.http.post(`${this.url}${this.paths.changeRequest}`, body.toString(), httpOptions)
          .subscribe((answer: any) => {
            if (answer.code == "200") {
              resolve("La solicitud se actualizó correctamente");
            }
            else{
              reject(answer.message);
            }
          }, (error: any) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

}
