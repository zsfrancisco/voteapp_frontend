// This service controls the different types of data in the application

import { Injectable } from '@angular/core';
import moment from 'moment';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  // this function returns a Date string valid to up in service petition
  dateToUP(date: NgbDateStruct): string {
    try {
      return `${date.day}/${date.month}/${date.year}`;
    } catch (error) {
      // console.log('error toDate: ', error);
    }
  }

  // this function uses moment library to transforms the string in a new Date
  dateToGet(value: String): NgbDateStruct {
    let date = moment(`${value}`, 'YYYY-M-D');
    // console.log('moment: ', date);
    return { day: parseInt(date.format('D')), month: parseInt(date.format('M')), year: parseInt(date.format('YYYY')) };
  }

  // hourToGet returns the hor for the form
  hourToGet(value: string) {
    const hour = moment(`${value}`, 'YYYY-MM-DD HH:mm:ss').format('HH');
    // console.log('hour', hour);
    const minute = moment(`${value}`, 'YYYY-MM-DD HH:mm:ss').format('mm');
    // console.log('minute', minute);
    return { hour: parseInt(hour), minute: parseInt(minute) }
  }
  // this function returns a string to show dates in tables
  dateToShow(value: String): String {
    let date = moment(`${value}`, 'D/M/YYYY').format('D[/]M[/]YYYY');
    return date;
  }

  // This function returns a simple date
  newDate(): NgbDateStruct {
    let newDate = moment();
    return { day: parseInt(newDate.format('D')), month: parseInt(newDate.format('M')), year: parseInt(newDate.format('YYYY')) }
  }

  // hour returns the current hour
  hour() {
    return moment().format('YYYY-MM-DD HH:mm:ss');
  }


}
