// this service controls the logic for the data of the candidates

import { Injectable } from '@angular/core';
import { NewUserFormBodyInterface, UserReturnedInterface } from '@interfaces';
import { AuthService } from './auth.service';
import { URL_API } from '../common/constants';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = URL_API;

  // Paths petitons
  private paths: any = {
    user: 'user'
  }

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private sanitizer: DomSanitizer,
  ) { }

  // newCandidate saves a candidate record in a specific process
  newUser(petition: NewUserFormBodyInterface) {
    let token: string = this.authService.readToken();
    const headers = { 'Authorization': token };

    const formData = new FormData();
    formData.append('json', JSON.stringify(petition.json));    

    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.user}`, formData, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            console.log('respuesta nuevo usuario: ', answer);
            resolve(answer);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }
}
