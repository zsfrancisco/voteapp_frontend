// This service controls the logic for encrypt a ip address

import { Injectable } from '@angular/core';
import CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncryptionService {

  // secret key
  encryptSecretKey = '123';
  
  constructor() { }

  // encryptData encrypt the data
  encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), this.encryptSecretKey).toString();
    } catch (e) {
      console.log('Error in encryption service function encryptData',e);
    }
  }

  // decryptData decrypt the data
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, this.encryptSecretKey);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log('Error in encryption service function decryptData',e);
    }
  }

}