import { Injectable } from '@angular/core';
import { RegisterVotePetitionInterface, VoterServiceReturnedInterface, VoterGetToken } from '@interfaces';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { URL_API } from '../common/constants';

@Injectable({
  providedIn: 'root'
})
export class VoterService {

  // Token
  private token: string = this.authService.readToken();

  private url = URL_API;

  // Petition paths
  private paths: any = {
    voter: 'voter',
    registerVote: 'voter/registerd_vote',
    getToken: 'voter/get_token',
  }

  constructor(
    private http: HttpClient,
    private authService: AuthService,
  ) { }

  // Returns all of Processes
  getVoters() {
    // let token: string = this.authService.readToken();
    // Headers petition
    const headers = { 'Authorization': this.token };
    // promise resolves the answer when the code is not 400
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}${this.paths.voter}`, { headers })
        .subscribe((answer: any) => {
          if (answer.code === '400') {
            reject(answer);
          }
          else {
            resolve(answer.voter as VoterServiceReturnedInterface[]);
          }
        }, (error: any) => {
          reject(error);
        });
    });
  }

  // Returns a specific process by Id
  getVoter(voterIdentification: string) {
    return new Promise(async (resolve, reject) => {
      try {
        const voters = await this.getVoters();
        (voters as VoterServiceReturnedInterface[]).forEach(voter => {
          if (voter.identification == voterIdentification) resolve(voter);
        });
      } catch (error) {
        reject(`No se encuentra el proceso con la identificación: ${voterIdentification}`);
      }
    });
  }

  // registerVote saves a vote for one candidate
  registerVoter(petition: RegisterVotePetitionInterface) {
    return new Promise(async (resolve, reject) => {
      try {
        // Petition to build the json in the body
        const body = new HttpParams()
          .set('json', JSON.stringify(petition));
        // HttpOptions petition
        const httpOptions = {
          headers: new HttpHeaders({
            'Authorization': this.token,
            'Content-Type': 'application/x-www-form-urlencoded'
          })
        };
        console.log('petición: ', petition, `url: ${this.url}${this.paths.registerVote}`);
        // Promise returns the resolve when the answer code is 200
        this.http.post(`${this.url}${this.paths.registerVote}`, body.toString(), httpOptions)
          .subscribe((answer: any) => {
            if (answer.code === '200') {
              resolve(answer);
            }
            else {
              reject(answer);
            }
          }, (error: any) => {
            reject(error);
          });
      } catch (error) {
        reject(error);
      }
    });
  }

  getToken(petition: VoterGetToken) {
    let params = new HttpParams()
      .set('json', JSON.stringify(petition));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': this.token
      })
    };
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}${this.paths.getToken}`, params, httpOptions)
        .subscribe((answer: any) => {
          console.log('respuesta de getToken: ', answer);
          
          answer.code === '200' ? resolve(answer) : reject(answer.message);
        }, (error: any) => {
          console.log('respuesta de catch getToken: ', error);
          reject(error.error.message);
        });
    });
  }

}
