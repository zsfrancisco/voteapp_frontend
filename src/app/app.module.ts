import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ChartsModule } from 'ng2-charts';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

import { AppComponent } from './app.component';
import { LoginAdminComponent } from './pages/admin/login-admin/login-admin.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { MainBannerComponent } from './components/banners/main-banner/main-banner.component';
import { NewProcessFormComponent } from './components/forms/new-process-form/new-process-form.component';
import { LoginAdminFormComponent } from './components/forms/login-admin-form/login-admin-form.component';
import { NewProcessComponent } from './pages/process/new-process/new-process.component';
import { PanelProcessComponent } from './pages/process/panel-process/panel-process.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { NewCandidateComponent } from './pages/candidates/new-candidate/new-candidate.component';
import { NewCandidateFormComponent } from './components/forms/new-candidate-form/new-candidate-form.component';
import { PanelCandidatesComponent } from './pages/candidates/panel-candidates/panel-candidates.component';
import { HomeComponent } from './pages/home/home.component';
import { GridProcessComponent } from './components/home/grid-process/grid-process.component';
import { ConsultSuitableComponent } from './components/home/consult-suitable/consult-suitable.component';
import { ReportNewsComponent } from './components/home/report-news/report-news.component';
import { ProcessesPermittedComponent } from './pages/voters/processes-permitted/processes-permitted.component';
import { VoterReportNewsComponent } from './pages/voters/voter-report-news/voter-report-news.component';
import { ReportNewsFormComponent } from './components/forms/report-news-form/report-news-form.component';
import { AboutComponent } from './pages/about/about.component';
import { TypewriterBannerComponent } from './components/banners/typewriter-banner/typewriter-banner.component';
import { VoteGridComponent } from './pages/process/vote-grid/vote-grid.component';
import { CandidatesProcessComponent } from './pages/process/candidates-process/candidates-process.component';
import { AccessVoteProcessComponent } from './pages/process/access-vote-process/access-vote-process.component';
import { AccessVoteProcessFormComponent } from './components/forms/access-vote-process-form/access-vote-process-form.component';
import { ResultProcessComponent } from './pages/process/result-process/result-process.component';
import { ErrorAccessComponent } from './pages/process/error-access/error-access.component';
import { EditCandidateComponent } from './pages/candidates/edit-candidate/edit-candidate.component';
import { EditProcessComponent } from './pages/process/edit-process/edit-process.component';
import { ResultGraphicsProcessComponent } from './pages/process/result-graphics-process/result-graphics-process.component';
import { PanelProposalsComponent } from './pages/candidates/panel-proposals/panel-proposals.component';
import { CheckCertificateComponent } from './components/home/check-certificate/check-certificate.component';
import { PanelAuthLogComponent } from './pages/process/panel-auth-log/panel-auth-log.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { CreateUserComponent } from './pages/users/create-user/create-user.component';
import { NewUserFormComponent } from './components/forms/new-user-form/new-user-form.component';
import { HomeCandidateComponent } from './pages/candidates/home-candidate/home-candidate.component';
import { CandidateReportNewsComponent } from './pages/candidates/candidate-report-news/candidate-report-news.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginAdminComponent,
    NavbarComponent,
    FooterComponent,
    MainBannerComponent,
    NewProcessComponent,
    NewProcessFormComponent,
    LoginAdminFormComponent,
    PanelProcessComponent,
    SpinnerComponent,
    NewCandidateComponent,
    NewCandidateFormComponent,
    PanelCandidatesComponent,
    HomeComponent,
    GridProcessComponent,
    ConsultSuitableComponent,
    ReportNewsComponent,
    ProcessesPermittedComponent,
    VoterReportNewsComponent,
    ReportNewsFormComponent,
    AboutComponent,
    TypewriterBannerComponent,
    VoteGridComponent,
    CandidatesProcessComponent,
    AccessVoteProcessComponent,
    AccessVoteProcessFormComponent,
    ResultProcessComponent,
    ErrorAccessComponent,
    EditCandidateComponent,
    EditProcessComponent,
    ResultGraphicsProcessComponent,
    PanelProposalsComponent,
    CheckCertificateComponent,
    PanelAuthLogComponent,
    RequestsComponent,
    CreateUserComponent,
    NewUserFormComponent,
    HomeCandidateComponent,
    CandidateReportNewsComponent,
  ],
  imports: [
    BrowserModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxDropzoneModule,
    NgbModule,
    ChartsModule,
    RecaptchaModule,
    RecaptchaFormsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
