// Service interfaces

// UserLoginPetitionInterface is used in the login petition
interface UserLoginPetitionInterface {
    user_name: string;
    password: string;
    ip_address: string;
}

// UserLoginReturnedInterface is the response for user login petition from the service
interface UserLoginReturnedInterface {
    status: string;
    code: string;
    token: string;
}

// TesisInformationInterface is used in the page about, used for show the information of the tesis study
interface TesisInformationInterface {
    title: string;
    cont: string;
}

export {
    UserLoginPetitionInterface,
    UserLoginReturnedInterface,
    TesisInformationInterface
}