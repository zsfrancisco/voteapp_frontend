// Contains all of the interfaces in the system

export * from './service.interfaces';
export * from './process.interfaces';
export * from './form.interfaces';
export * from './candidate.interfaces';
export * from './report-news.interfaces';
export * from './developer.interfaces';
export * from './voter.interfaces';
export * from './voter-request.interface';
export * from './user.interfaces';