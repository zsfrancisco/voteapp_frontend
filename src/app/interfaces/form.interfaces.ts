// form interfaces 
// Contains the interfaces used in forms, generally default options

// TwoOptionInterface is an object with two options: value and viewValue
interface TwoOptionInterface {
    value:     string;
    viewValue: String;
}

export {
    TwoOptionInterface
}