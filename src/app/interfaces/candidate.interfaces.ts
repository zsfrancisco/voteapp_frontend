// Candidates interfaces

// electoralProposalsInterface: cadidate's proposals
// it's used inside JsonCandidateFormBodyInterface
interface electoralProposalsInterface {
    generalDescription: string;
    details: string;
}

// JsonCandidateFormBodyInterface is the variable "json" inside NewCandidateFormBodyInterface
interface JsonCandidateFormBodyInterface {
    identification: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    description: string;
    voting_process_id: string;
    proposals: electoralProposalsInterface[]
}

// NewCandidateFormBodyInterface is the form body for create a new candidate
interface NewCandidateFormBodyInterface {
    json: JsonCandidateFormBodyInterface;
    file0: File;
}

// CandidatesPivotInterface is part of the response from the server of a candidate, 
// and it's used inside CandidateReturnedInterface
interface CandidatesPivotInterface {
    voting_process_id: string;
    candidate_id: string;
}

// ProposalsReturnedInterface is part of the response from the server of a candidate, 
// and it's used inside CandidateReturnedInterface
interface ProposalsReturnedInterface {
    generalDescription: string;
    details: string;
}

// CandidateReturnedInterface is the respondse of a candidate from the service
interface CandidateReturnedInterface {
    id: string;
    identification: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    description: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
    pivot: CandidatesPivotInterface;
    proposals: ProposalsReturnedInterface[];
}

// CandidateVoteDetailsTemplateInterface is the body for card in the candidates review page
interface CandidateVoteDetailsTemplateInterface {
    id: string;
    identification: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    description: string;
    pivot: CandidatesPivotInterface;
    proposals: ProposalsReturnedInterface[];
    image: any,
}

// CandidateHomeInformation is the interface used to show the information of the candidate loged
interface CandidateHomeInformation {
    id: string,
    identification: string,
    first_name: string,
    last_name: string,
    birthdate: string,
    gender: string,
    email: string,
}

// CandidateRequest interface used to do a candidate request
interface CandidateRequest {
    voting_process_id: string;
    candidate_id: string;
    comments: string;
}

// CandidateRequestReturned interface used to show the information of a candidate request
interface CandidateRequestReturned {
    id: string;
    candidate_id: string;
    voting_process_id: string;
    comments: string;
    status: string;
    reply: string;
    custom_data: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
    voting_process_name: string;
}

// JsonChangeCandidateRequest interface used to change the status of a candidate request
interface JsonChangeCandidateRequest {
    candidate_request_id: string;
    status: any;
    reply: any;
}

// JsonCandidateFormBodyInterface is the variable "json" inside NewCandidateFormBodyInterface
interface JsonEditCandidateFormBody {
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    description: string;
    // proposals: electoralProposalsInterface[]
}

// NewCandidateFormBodyInterface is the form body for create a new candidate
interface EditCandidateFormBody {
    json: JsonEditCandidateFormBody;
    file0: File;
}

export {
    NewCandidateFormBodyInterface,
    electoralProposalsInterface,
    JsonCandidateFormBodyInterface,
    CandidateReturnedInterface,
    CandidatesPivotInterface,
    ProposalsReturnedInterface,
    CandidateVoteDetailsTemplateInterface,
    CandidateHomeInformation,
    CandidateRequest,
    CandidateRequestReturned,
    JsonChangeCandidateRequest,
    JsonEditCandidateFormBody,
    EditCandidateFormBody,
}