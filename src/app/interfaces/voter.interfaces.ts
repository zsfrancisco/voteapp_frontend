// VoterServiceReturnedInterface is the information of the voter returned in service
interface VoterServiceReturnedInterface {
    id: string;
    identification: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    custom_data: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
}

interface RegisterVotePetitionInterface {
    voting_process_id: string;
    voter_id: string;
}

interface VoterGetToken {
    voting_process_id: string;
    voter_identification: string;
    ip_address: string;
}

export {
    VoterServiceReturnedInterface,
    RegisterVotePetitionInterface,
    VoterGetToken,
}