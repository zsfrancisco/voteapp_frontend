// User interfaces

// JsonUserFormBodyInterface is the variable "json" inside JsonUserFormBodyInterface
interface JsonUserFormBodyInterface {
    identification: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    user_name: string;
    password: string;
    role_id: string;
}

// NewCandidateFormBodyInterface is the form body for create a new candidate
interface NewUserFormBodyInterface {
    json: JsonUserFormBodyInterface;    
}

// CandidateReturnedInterface is the respondse of a candidate from the service
interface UserReturnedInterface {
    id: string;
    identification: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    gender: string;
    email: string;
    username: string;    
    created_at: string;
    updated_at: string;
    deleted_at: string;    
}

export {
    JsonUserFormBodyInterface,
    NewUserFormBodyInterface,
    UserReturnedInterface
}