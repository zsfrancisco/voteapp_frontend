// Report news interfaces

// NewReportNewsFormBodyInterface is used in the form to new report news
interface NewReportNewsFormBodyInterface {
    identification: string;
    first_name: string;
    last_name: string;
    email: string;
    comments: string;
    voting_process_id: string;
}

export {
    NewReportNewsFormBodyInterface,
}