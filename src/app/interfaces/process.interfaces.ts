// Contains all of process interfaces

// NewProcessFormBodyInterface is the interface used in the form for create a new electoral process
interface NewProcessFormBodyInterface {
    file_users: File
    json: JsonProcessBodyInterface;
    file0: File;
}

// EditProcessFormBodyInterface is the interface used in the form for edit an electoral process
interface EditProcessFormBodyInterface {
    file_users: File
    json: JsonProcessBodyInterface;
    file0: File;
}

// JsonProcessBodyInterface is part of NewProcessFormBodyInterface and contains the information of the process
interface JsonProcessBodyInterface {
    name: string;
    description: string;
    start_date: string;
    end_date: string;
    user_id: string;
}

// EditProcessBodyInterface is the interface for edit a electoral process
interface EditProcessBodyInterface {
    name: string;
    description: string;
    start_date: string;
    end_date: string;
    user_id: string;
    status: string;
}

// ChangeStatusProcessInterface is the interface for change the status of a electoral process
interface ChangeStatusProcessInterface {
    user_id: string,
    voting_process_id: string,
    status: string;
}

// ProcessReturned is the process interface that the service returns
interface ProcessReturned {
    candidates_number: string;
    created_at: string;
    deleted_at: string;
    description: string;
    end_date: string;
    id: string;
    image: string;
    name: string;
    start_date: string;
    status: string;
    updated_at: string;
    user_id: string;
}

// ProcessSuitableForUserInterface is used for consult the allowed processes
interface ProcessSuitableForUserInterface {
    name: string;
    description: string;
    start_date: string;
}

// ProcessCardTemplateInterface is used in the template for show a process
interface ProcessCardTemplateInterface {
    id: string;
    name: string;
    description: string;
    start_date: string;
    image: any;
    status: string;
}

// RegisterVoteInterface interface used to register a vote
interface RegisterVoteInterface {
    voting_process_id: string;
    candidate_id: string;
    voter_id: string;
    ip_address: string;
}

// PublicProcessResultInterface interface used to show a process result
interface PublicProcessResultInterface {
    first_name: string;
    last_name: string;
    votes_count: string;
}

// IAuthenticationLog interface used to show an authentication log
interface IAuthenticationLog {
    id: string;
    voter_identification: string;
    voting_process_id: string;
    score: string;
    status: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
}

export {
    NewProcessFormBodyInterface,
    JsonProcessBodyInterface,
    ProcessReturned,
    EditProcessBodyInterface,
    ProcessSuitableForUserInterface,
    ProcessCardTemplateInterface,
    ChangeStatusProcessInterface,
    RegisterVoteInterface,
    PublicProcessResultInterface,
    EditProcessFormBodyInterface,
    IAuthenticationLog,
}