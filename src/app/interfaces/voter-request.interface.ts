// IVoterRequest interface used to show a request of a voter
interface IVoterRequest {
    id: string;
    identification: string;
    first_name: string;
    last_name: string;
    email: string;
    comments: string;
    voting_process_id: string;
    status: string;
    reply: string;
    custom_data: string;
    created_at: string;
    updated_at: string;
    deleted_at: string;
    voting_process_name: string;
}

// IJsonChangeVoterRequest interface used to be "json" in IChangeVoterRequest
interface IJsonChangeVoterRequest {
    voter_request_id: string;
    status: any;
    reply: any;
}

// IChangeVoterRequest interface used to change a request of a voter
interface IChangeVoterRequest {
    json: IJsonChangeVoterRequest,
}

export {
    IVoterRequest,
    IJsonChangeVoterRequest,
    IChangeVoterRequest,
}