// Developer interfaces

// DeveloperTemplateInterface is the developer information,is used in the template in cards
interface DeveloperTemplateInterface {
    name: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    linkedIn: string;
    twitter: string;
    facebook: string;
    instagram: string;
    image: string;
}

export {
    DeveloperTemplateInterface,
}