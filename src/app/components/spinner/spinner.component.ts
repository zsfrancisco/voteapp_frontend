// this component controls the logic to show a spinner

import { Component, OnInit, Input } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  constructor(
    private spinner: NgxSpinnerService) { }

  @Input() divClass: string = 'div-spinner-default'

  ngOnInit(): void {
    // showing the spinner
    this.spinner.show();
  }

}
