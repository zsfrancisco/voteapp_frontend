import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  imageLogo: string = 'assets/img/00a896.png'
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    // this.isAuth;
  }

  isHover(value: boolean) {
    value ? this.imageLogo = 'assets/img/02c39a.png' : this.imageLogo = 'assets/img/00a896.png';
  }

  // get isAuth() {
  //   return this.authService.isAuth();
  // }

  // reportNews return true if the user has the role 1
  isAdmin(): boolean {
    return this.authService.readRoleId() == '1';
  }

  isCandidate(): boolean {
    return this.authService.readRoleId() == '2';
  }

  // logOut is the function to destroy the session in localstorage
  logOut() {
    this.authService.logout();
    // this.router.navigateByUrl('/login');
    this.router.navigateByUrl('/home');
  }

}
