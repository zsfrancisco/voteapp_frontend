// This component shows a type writer effect in the template

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-typewriter-banner',
  templateUrl: './typewriter-banner.component.html',
  styleUrls: ['./typewriter-banner.component.css']
})
export class TypewriterBannerComponent implements OnInit {

  // typewriter_text is the text that I want to show
  private typewriter_text: string = "Tácticas arquitectónicas de seguridad para e-voting.";
  // typewriter_text is the text that is showed in template
  public typewriter_display: string = "";

  constructor() { }

  ngOnInit(): void {
    this.typingCallback(this);
  }

  // typingCallback is the funtion that do the type writer effect
  typingCallback(that) {
    let total_length = that.typewriter_text.length;
    let current_length = that.typewriter_display.length;
    if (current_length < total_length) {
      that.typewriter_display += that.typewriter_text[current_length];
      setTimeout(that.typingCallback, 100, that);
    }  
  }

}
