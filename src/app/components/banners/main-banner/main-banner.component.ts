// This component controls the template of a banner, this banner can show buttons
// all the parameters come from the call with input

import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-banner',
  templateUrl: './main-banner.component.html',
  styleUrls: ['./main-banner.component.css']
})
export class MainBannerComponent implements OnInit {

  // Text center in banner
  @Input() text: string = '';
  
  // Button 1
  @Input() textButtonNew: string = null;
  // Route for button 1
  @Input() routeButtonNew: string = null;
  
  // Button 2
  @Input() textButtonDownload: string = null;
  @Input() routeButtonDownload: string = null;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  downloadReport() {
    console.log('click función downloadReport');
  }

  funNew() {
    this.router.navigateByUrl(this.routeButtonNew);
  }
  funReport() {
    // console.log('Ruta reporte: ', this.routeButtonDownload);
    window.open(this.routeButtonDownload, '_blank');
  }

}
