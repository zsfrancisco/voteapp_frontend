import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginAdminFormComponent } from './login-admin-form.component';

describe('LoginAdminFormComponent', () => {
  let component: LoginAdminFormComponent;
  let fixture: ComponentFixture<LoginAdminFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginAdminFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAdminFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
