// This component controls the login of the administrator user in the system

import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { Router } from '@angular/router';
import { UserLoginPetitionInterface } from '@interfaces';
import { AuthService } from '../../../services/auth.service';
import { IpAddressService } from '../../../services/ip-address.service';

@Component({
  selector: 'app-login-admin-form',
  templateUrl: './login-admin-form.component.html',
  styleUrls: ['./login-admin-form.component.css']
})
export class LoginAdminFormComponent implements OnInit {

  form: FormGroup;

  // Ip address of the current device
  ipAddress: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    private authService: AuthService,
    private ipAddressService: IpAddressService
  ) { }

  ngOnInit(): void {
    this.getIpAddress();
    this.createForm();
  }

  // This function creates a form to with login inputs
  createForm() {
    this.form = this.formBuilder.group({
      user: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  // Getters not valid:
  // When an input of the form is invalid, it is called
  get userNotValid() {
    return this.form.get('user').invalid && this.form.get('user').touched;
  }
  get passwordNotValid() {
    return this.form.get('password').invalid && this.form.get('password').touched;
  }

  // Get the ip address of the current device
  getIpAddress() {
    this.ipAddressService.getIPAddress().subscribe((res:any)=>{  
      this.ipAddress=res.ip;  
    });  
  }

  // Form submit
  async onSubmit() {
    // Form is invalid
    if (this.form.invalid) {
      return Object.values(this.form.controls).forEach(control => {
        control.markAsTouched();
      });
    }
    // Form is valid
    this.sweetAlertService.loadingAlert('espere');
    // petition to enter to the system
    let petition: UserLoginPetitionInterface = {
      user_name: this.form.controls.user.value,
      password: this.form.controls.password.value,
      ip_address: this.ipAddress
    }
    // Loging
    try {
      let userAuth: any = await this.authService.loginUser(petition);
      if (userAuth && userAuth.code === "200") {
        let token = userAuth.token;
        let userId = userAuth.user_id;
        let roleId = userAuth.role;
        this.authService.saveSession(token, userId, roleId);
        this.sweetAlertService.succesAlert('¡Usuario logueado!');
        setTimeout(() => {
          this.router.navigateByUrl('home');
        }, 1500);
      } else {
        this.sweetAlertService.errorAlert('No se pudo loguear el usuario');
      }
    } catch (error) {
      // console.log(error);
      this.sweetAlertService.errorAlert(error.error.message);
    }
  }

}