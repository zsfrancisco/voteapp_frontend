import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessVoteProcessFormComponent } from './access-vote-process-form.component';

describe('AccessVoteProcessFormComponent', () => {
  let component: AccessVoteProcessFormComponent;
  let fixture: ComponentFixture<AccessVoteProcessFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessVoteProcessFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessVoteProcessFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
