import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { ProcessService } from 'src/app/services/process.service';
import { VoterService } from '../../../services/voter.service';
import { VoterServiceReturnedInterface } from '@interfaces';

@Component({
  selector: 'app-access-vote-process-form',
  templateUrl: './access-vote-process-form.component.html',
  styleUrls: ['./access-vote-process-form.component.css']
})
export class AccessVoteProcessFormComponent implements OnInit {

  processId: string = '';

  form: FormGroup;

  votersIdetifications: string[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private sweetAlertService: SweetAlertService,
    private processService: ProcessService,
    private voterService: VoterService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => this.processId = params['processId']);
    this.createForm();
  }

  // This function creates a form to with login inputs
  createForm() {
    this.form = this.formBuilder.group({
      voter: ['', [Validators.required]],
    });
  }

  // Getters not valid:
  // When an input of the form is invalid, it is called
  get voterNotValid() {
    return this.form.get('voter').invalid && this.form.get('voter').touched;
  }

  // Form submit
  async onSubmit() {
    // Form is invalid
    if (this.form.invalid) {
      return Object.values(this.form.controls).forEach(control => {
        control.markAsTouched();
      });
    }
    // Form is valid
    this.sweetAlertService.loadingAlert('espere');
    // petition to enter to the system
    const voterIdentification: string = this.form.controls.voter.value;
    // Loging
    try {
      this.votersIdetifications = [];
      const votersList: string[] = await this.processService.votersLabels(this.processId) as string[];
      const voterIdentification: string = this.form.controls.voter.value;
      const index = votersList.indexOf(voterIdentification);
      if ( index >= 0 ) {
        const voter: VoterServiceReturnedInterface = await  this.voterService.getVoter(voterIdentification) as VoterServiceReturnedInterface;
        this.authService.accessVote(this.processId, voter.id);
        this.sweetAlertService.succesAlert('¡Ciudadano logueado!');
        setTimeout(() => {
          this.router.navigateByUrl(`/process/${this.processId}/vote`);
        }, 1500);
      } else {
        this.sweetAlertService.errorAlert(`El ciudadano con identificación ${voterIdentification}, no puede votar en este proceso de elección`);
      }
    } catch (error) {
      console.log(error);
      this.sweetAlertService.errorAlert(error.error.message);
    }
  }

}
