// This component manages the log of creation of a candidate to a specific process

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DateService } from 'src/app/services/date.service';
import { TwoOptionInterface, electoralProposalsInterface, NewCandidateFormBodyInterface, JsonCandidateFormBodyInterface, CandidateReturnedInterface, JsonEditCandidateFormBody, EditCandidateFormBody } from '@interfaces';
import { DefaultOptionsService } from '../../../services/default-options.service';
import Swal from 'sweetalert2';
import { CandidateService } from '../../../services/candidate.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-candidate-form',
  templateUrl: './new-candidate-form.component.html',
  styleUrls: ['./new-candidate-form.component.css']
})
export class NewCandidateFormComponent implements OnInit {

  // Form
  form: FormGroup;

  // file input image html reference
  @ViewChild('fileInputImage') fileInputImage;

  // Image file to upload
  fileToUploadImage: File = null;

  // Candidate photo in the template
  candidatePhoto: string = 'assets/img/default-image.jpg';

  // Gender options - M: Masculino, F: Femenino
  genderOptions: TwoOptionInterface[] = this.defaultOptionsService.genders;

  // Interface for electoral proposals
  electoralProposals: electoralProposalsInterface[] = [];

  // Process to record the candidate
  processId: string;

  // Min date to the candidate birthday
  minDate: NgbDateStruct = { year: 1920, month: 1, day: 1 };

  @Input() isEdit = false;
  currentCandidateId: string = "";

  isLoading = false;
  isError = false;

  constructor(
    private formBuilder: FormBuilder,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    private dateService: DateService,
    private defaultOptionsService: DefaultOptionsService,
    private activateRoute: ActivatedRoute,
    private candidateService: CandidateService,
  ) { }

  ngOnInit(): void {
    // Getting the processId from the route
    this.activateRoute.params.subscribe(params => {
      this.processId = params['processId'];
      this.currentCandidateId = params['candidateId'];
    });
    this.isEdit ? this.getCandidateInformation() : this.createForm();
  }

  // getCandidateInformation gets the information of a candidate
  async getCandidateInformation() {
    try {
      this.isLoading = true;
      this.isError = false;
      const candidate = await this.candidateService.getCandidateById(this.currentCandidateId) as CandidateReturnedInterface;
      console.log("Candidato: ", candidate);
      const birthday = this.dateService.dateToGet(candidate.birthdate)
      this.createForm(
        candidate.first_name,
        candidate.last_name,
        { value: candidate.identification, disabled: true },
        birthday,
        candidate.email,
        candidate.gender,
        candidate.description,
      );
      (candidate.proposals).forEach(proposal => {
        this.electoralProposals.push(proposal);
      });
      const candidateImage = await this.candidateService.getCandidateImage(this.processId, candidate.identification) as string;
      this.candidatePhoto = candidateImage;
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect("Ocurrió un error cargando los datos del candidato, inténtelo más tarde", "/process/panel");
    }
  }

  // Creates a form with the fields
  createForm(
    firstName = "",
    lastName = "",
    identification = { value: '', disabled: false },
    birthday = this.dateService.newDate(),
    email = "",
    gender = "M",
    description = "",
  ) {
    const validatorImg = this.isEdit ? [] : [Validators.required];
    this.form = this.formBuilder.group({
      firstName: [firstName, [Validators.required]],
      lastName: [lastName, [Validators.required]],
      identification: [identification, [Validators.required]],
      birthday: [birthday, [Validators.required]],
      email: [email, [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      gender: [gender, [Validators.required]],
      description: [description, [Validators.required]],
      image: ['', validatorImg],
    });
    this.isLoading = false;
  }

  // GETS NOT VALID STARTS
  get firstNameNotValid() {
    return this.form.get('firstName').invalid && this.form.get('firstName').touched;
  }
  get lastNameNotValid() {
    return this.form.get('lastName').invalid && this.form.get('lastName').touched;
  }
  get identificationNotValid() {
    return this.form.get('identification').invalid && this.form.get('identification').touched;
  }
  get birthdayNotValid() {
    return this.form.get('birthday').invalid && this.form.get('birthday').touched;
  }
  get emailNotValid() {
    return this.form.get('email').invalid && this.form.get('email').touched;
  }
  get genderNotValid() {
    return this.form.get('gender').invalid && this.form.get('gender').touched;
  }
  get descriptionNotValid() {
    return this.form.get('description').invalid && this.form.get('description').touched;
  }
  get imageNotValid() {
    return this.form.get('image').invalid && this.form.get('image').touched;
  }
  // GETS NOT VALID ENDS

  // Function that runs when a file is selected from the template and will be the log image
  handleFileInput(files: FileList) {
    if (files.item(0).type === 'image/jpg' || files.item(0).type === 'image/jpeg') {
      this.fileToUploadImage = files.item(0);
      let reader = new FileReader;
      reader.onload = (e: any) => {
        this.candidatePhoto = e.target.result;
      };
      reader.readAsDataURL(this.fileToUploadImage);
    } else {
      this.candidatePhoto = 'assets/img/image-error-1.jpg';
      this.sweetAlertService.errorAlert('Verifique que el archivo sea tipo jpg, jpeg o png y vuelva a intentarlo');
      console.log(files.item(0).type)
    }
  }

  // Funcion that record a proposal in the proposals array of the candidate
  async handleElectoralProposals() {
    const { value: formValues } = await Swal.fire({
      title: '<span class="font-028090">Agregar propuesta<span/>',
      html:
        `
        <input id="generalDescription" 
              class="swal2-input"
              placeholder="Descripción general"
              required>
        <textarea class="swal2-textarea" 
                  placeholder="Descripción general del candidato que se va a postular." 
                  name="descriptionDetails"
                  id="descriptionDetailsId" 
                  rows="4"
                  required></textarea>
        `,
      confirmButtonText: "Agregar propuesta",
      cancelButtonText: '<span class="font-05668D">Descartar cambios</span<',
      confirmButtonColor: '#05668d',
      cancelButtonColor: '#ffffff',
      showCancelButton: true,
      focusConfirm: false,
      reverseButtons: true,
      preConfirm: () => {
        if ((document.getElementById('generalDescription') as HTMLInputElement).value != "" &&
          (document.getElementById('descriptionDetailsId') as HTMLInputElement).value != "") {
          return ([
            (document.getElementById('generalDescription') as HTMLInputElement).value,
            (document.getElementById('descriptionDetailsId') as HTMLInputElement).value
          ])
        } else {
          Swal.showValidationMessage('La descripción general y específica son obligatorias');
        }
      }
    });

    // Pushing the proposal
    if (formValues) {
      this.electoralProposals.push({ generalDescription: formValues[0], details: formValues[1] });
    }

  }

  // View Proposal detail
  viewProposal(proposal: string) {
    if (this.electoralProposals.length > 0) {
      console.log('Si hay al menos un registro');

      const proposalSearched = this.electoralProposals.find(prop => prop.generalDescription === proposal);
      if (proposalSearched) {
        Swal.fire({
          title: '<span class="font-028090">Detalle de la propuesta<span/>',
          html: `
            <div class="container text-left">
              <span class="font-05668D font-weight-bold">Título:</span> <span>${proposalSearched.generalDescription}</span>
              <br><span class="font-05668D font-weight-bold">Descripción: </span><span>${proposalSearched.details}</span>
            </div>
          `,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
          confirmButtonColor: '#05668d',
        });
      }
    }
  }

  // Delete all of the proposals
  deleteAllProposal() {
    if (this.electoralProposals.length > 0) this.electoralProposals = [];
  }

  // Delete a specific proposal
  deleteProposal(proposal: string) {
    if (this.electoralProposals.length > 0) {
      const idx = this.electoralProposals.findIndex(prop => prop.generalDescription === proposal);
      if (idx !== -1) {
        this.electoralProposals.splice(idx, 1);
      }
    }
  }

  // Recording the candidadte in the data base
  async onSubmit() {
    // Validating form
    if (this.form.invalid || this.electoralProposals.length < 1 || (!this.isEdit && !this.fileToUploadImage)) {
      if (!this.isEdit && !this.fileToUploadImage) {
        this.candidatePhoto = 'assets/img/image-error-1.jpg';
      }
      if (this.electoralProposals.length < 1) {
        this.sweetAlertService.errorAlert('Debe incluir al menos una propuesta electoral');
      }
      return Object.values(this.form.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    this.isEdit ? await this.editCandidate() : this.createCandidate();
  }

  async createCandidate() {
    this.sweetAlertService.loadingAlert('Registrando el candidato');

    // Variables to jsonPetition
    const birthdateForm: NgbDateStruct = this.form.controls.birthday.value;
    const birthdatePetition: string = `${birthdateForm.year}-${birthdateForm.month}-${birthdateForm.day}`;

    // jsonPetition for petition
    let jsonPetition: JsonCandidateFormBodyInterface = {
      identification: this.form.controls.identification.value,
      first_name: this.form.controls.firstName.value,
      last_name: this.form.controls.lastName.value,
      birthdate: birthdatePetition,
      gender: this.form.controls.gender.value,
      email: this.form.controls.email.value,
      description: this.form.controls.description.value,
      voting_process_id: this.processId,
      proposals: this.electoralProposals,
    };

    // petition for the service
    let petition: NewCandidateFormBodyInterface = {
      json: jsonPetition,
      file0: this.fileToUploadImage,
    };

    // console.log('petición: ', JSON.stringify(petition.json), 'imagen', petition.file0);
    try {
      // recording the candidate
      let process = await this.candidateService.newCandidate(petition);
      if (process) {
        let message: string = 'Candidato postulado satisfactoriamente';
        this.sweetAlertService.succesAlert(message);
        setTimeout(() => {
          this.router.navigateByUrl(`/process/${this.processId}/candidates-panel`);
        }, 1500);
      } else {
        console.log('error línea 153');
      }
    } catch (error) {
      console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlert('No se pudo postular el candidato al proceso de votación, intente más tarde');
    }
  }

  async editCandidate() { 
    this.sweetAlertService.loadingAlert('Modificando los datos  del candidato');

    // Variables to jsonPetition
    const birthdateForm: NgbDateStruct = this.form.controls.birthday.value;
    const birthdatePetition: string = `${birthdateForm.year}-${birthdateForm.month}-${birthdateForm.day}`;

    // jsonPetition for petition
    let jsonPetition: JsonEditCandidateFormBody = {
      first_name: this.form.controls.firstName.value,
      last_name: this.form.controls.lastName.value,
      birthdate: birthdatePetition,
      gender: this.form.controls.gender.value,
      email: this.form.controls.email.value,
      description: this.form.controls.description.value,
      // proposals: this.electoralProposals,
    };

    // petition for the service
    let petition: EditCandidateFormBody = {
      json: jsonPetition,
      file0: this.fileToUploadImage,
    };

    // console.log('petición: ', JSON.stringify(petition.json), 'imagen', petition.file0);
    try {
      // recording the candidate
      let process = await this.candidateService.editCandidate(petition, this.currentCandidateId);
      if (process) {
        let message: string = 'Los datos del candidato se modificaron satisfactoriamente';
        this.sweetAlertService.succesAlert(message);
        setTimeout(() => {
          this.router.navigateByUrl(`/process/${this.processId}/candidates-panel`);
        }, 1500);
      } else {
        console.log('error línea 344');
      }
    } catch (error) {
      console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlert('No se pudo modificar los datos del candidato al proceso de votación, intente más tarde');
    }
  }

  // redirects to route: candidates panel
  cancel() {
    this.router.navigateByUrl(`/process/${this.processId}/candidates`);
  }

}
