import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCandidateFormComponent } from './new-candidate-form.component';

describe('NewCandidateFormComponent', () => {
  let component: NewCandidateFormComponent;
  let fixture: ComponentFixture<NewCandidateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCandidateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCandidateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
