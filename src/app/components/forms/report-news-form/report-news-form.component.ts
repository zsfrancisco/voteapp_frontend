import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProcessReturned, NewReportNewsFormBodyInterface, CandidateRequest } from '@interfaces';
import { ProcessService } from 'src/app/services/process.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-report-news-form',
  templateUrl: './report-news-form.component.html',
  styleUrls: ['./report-news-form.component.css']
})
export class ReportNewsFormComponent implements OnInit {

  // Form
  form: FormGroup;

  // Process
  processesArray: ProcessReturned[] = [];
  processDefault: string = '';

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;

  @Input() isCandidate = false;
  @Input() votingProcess: string = "";
  @Input() candidateId: string = "";

  constructor(
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.isCandidate ?
      this.createCandidateForm() :
      this.loadProcess()
  }

  // loadProcess: fill the processes array only with process status = 0
  async loadProcess() {
    this.isLoading = true;
    this.isError = false;
    this.isEmpty = false;
    this.processesArray = [];
    try {
      // Getting all of the processes
      let processprov = await this.processService.getProcesses();
      // Evaluating the state
      (processprov as ProcessReturned[]).forEach(process => {
        if (process.status == '0') {
          this.processesArray.push(process);
        }
      });
    } catch (error) {
      this.isError = true;
    }
    if (this.processesArray.length > 0) {
      this.isEmpty = false;
      this.processDefault = this.processesArray[0].id;
      this.createForm();
    } else {
      this.isEmpty = true;
      this.isLoading = false;
    }
    console.log('Procesos retorados: ', this.processes);
    console.log('Procesos por defecto: ', this.processDefault);
  }

  // processes returns the data with the pagination
  get processes(): any[] {
    return this.processesArray;
  }

  // Creates a form with the fields
  createForm() {
    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      identification: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      processId: [this.processDefault, [Validators.required]],
      comments: ['', [Validators.required]],
      recaptcha: [null, [Validators.required]],
    });
    this.isLoading = false;
  }

  resolvedCaptcha(event) {
    console.log("Evento: ", event)
  }

  // Creates a form with the fields
  createCandidateForm() {
    this.form = this.formBuilder.group({
      comments: ['', [Validators.required]],
    });
    this.isLoading = false;
  }

  // Not valid getters
  get firstNameNotValid() {
    return this.form.get('firstName').invalid && this.form.get('firstName').touched;
  }
  get lastNameNotValid() {
    return this.form.get('lastName').invalid && this.form.get('lastName').touched;
  }
  get identificationNotValid() {
    return this.form.get('identification').invalid && this.form.get('identification').touched;
  }
  get emailNotValid() {
    return this.form.get('email').invalid && this.form.get('email').touched;
  }
  get processIdNotValid() {
    return this.form.get('processId').invalid && this.form.get('processId').touched;
  }
  get commentsNotValid() {
    return this.form.get('comments').invalid && this.form.get('comments').touched;
  }

  // Recording the candidadte in the data base
  async onSubmit() {
    // Validating form
    if (this.form.invalid) {
      return Object.values(this.form.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    this.sweetAlertService.loadingAlert('Enviando su solicitud');

    this.isCandidate ?
      await this.candidateRequest() :
      await this.voterRequest();

  }

  async voterRequest() {
    // jsonPetition for petition
    const petition: NewReportNewsFormBodyInterface = {
      first_name: this.form.controls.firstName.value,
      last_name: this.form.controls.lastName.value,
      identification: this.form.controls.identification.value,
      email: this.form.controls.email.value,
      comments: this.form.controls.comments.value,
      voting_process_id: this.form.controls.processId.value,
    };

    console.log('petición: ', petition);
    try {
      // recording the candidate
      await this.processService.reportNews(petition);
      let message: string = 'Su solicitud satisfactoriamente';
      this.sweetAlertService.succesAlert(message);
      setTimeout(() => {
        this.router.navigateByUrl(`/`);
      }, 1500);
    } catch (error) {
      console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlertRedirect('No se pudo enviar su solicitud inténtelo más tarde', '/');
    }
  }

  async candidateRequest() {
    const petition: CandidateRequest = {
      voting_process_id: this.votingProcess,
      candidate_id: this.candidateId,
      comments: this.form.controls.comments.value,
    }
    try {
      // recording the candidate
      await this.processService.reportCandidateNews(petition);
      let message: string = 'Su solicitud satisfactoriamente';
      this.sweetAlertService.succesAlert(message);
      setTimeout(() => {
        this.router.navigateByUrl(`/`);
      }, 1500);
    } catch (error) {
      console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlertRedirect('No se pudo enviar su solicitud inténtelo más tarde', '/');
    }
  }

  // redirects to route: candidates panel
  cancel() {
    this.router.navigateByUrl(`/`);
  }

}
