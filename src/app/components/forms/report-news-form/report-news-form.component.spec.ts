import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportNewsFormComponent } from './report-news-form.component';

describe('ReportNewsFormComponent', () => {
  let component: ReportNewsFormComponent;
  let fixture: ComponentFixture<ReportNewsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportNewsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportNewsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
