import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { Router } from '@angular/router';
import { UserLoginPetitionInterface } from '@interfaces';
import { AuthService } from '../../../services/auth.service';
import { TwoOptionInterface, JsonUserFormBodyInterface, NewUserFormBodyInterface } from '@interfaces';
import { DefaultOptionsService } from '../../../services/default-options.service';
import { DateService } from 'src/app/services/date.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-new-user-form',
  templateUrl: './new-user-form.component.html',
  styleUrls: ['./new-user-form.component.css']
})
export class NewUserFormComponent implements OnInit {

  form: FormGroup;

  // Gender options - M: Masculino, F: Femenino
  genderOptions: TwoOptionInterface[] = this.defaultOptionsService.genders;

  // Min date to the candidate birthday
  minDate: NgbDateStruct = { year: 1970, month: 1, day: 1 };

  constructor(
    private formBuilder: FormBuilder,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    private authService: AuthService,
    private defaultOptionsService: DefaultOptionsService,
    private dateService: DateService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.createForm();
  }

  // Creates a form with the fields
  createForm() {
    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      identification: ['', [Validators.required]],
      birthday: [this.dateService.newDate(), [Validators.required]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      gender: ['M', [Validators.required]],
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  // GETS NOT VALID STARTS
  get firstNameNotValid() {
    return this.form.get('firstName').invalid && this.form.get('firstName').touched;
  }
  get lastNameNotValid() {
    return this.form.get('lastName').invalid && this.form.get('lastName').touched;
  }
  get identificationNotValid() {
    return this.form.get('identification').invalid && this.form.get('identification').touched;
  }
  get birthdayNotValid() {
    return this.form.get('birthday').invalid && this.form.get('birthday').touched;
  }
  get emailNotValid() {
    return this.form.get('email').invalid && this.form.get('email').touched;
  }
  get genderNotValid() {
    return this.form.get('gender').invalid && this.form.get('gender').touched;
  }
  get passwordNotValid() {
    return this.form.get('password').invalid && this.form.get('password').touched;
  }
  get userNameNotValid() {
    return this.form.get('userName').invalid && this.form.get('userName').touched;
  }

  // GETS NOT VALID ENDS


  // Recording the candidadte in the data base
  async onSubmit() {
    // Validating form
    if (this.form.invalid) {
      return Object.values(this.form.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    this.sweetAlertService.loadingAlert('Registrando el usuario');

    // Variables to jsonPetition
    const birthdateForm: NgbDateStruct = this.form.controls.birthday.value;
    const birthdatePetition: string = `${birthdateForm.year}-${birthdateForm.month}-${birthdateForm.day}`;

    // jsonPetition for petition
    let jsonPetition: JsonUserFormBodyInterface = {
      identification: this.form.controls.identification.value,
      first_name: this.form.controls.firstName.value,
      last_name: this.form.controls.lastName.value,
      birthdate: birthdatePetition,
      gender: this.form.controls.gender.value,
      email: this.form.controls.email.value,
      user_name: this.form.controls.userName.value,
      password: this.form.controls.password.value,
      role_id: "1",
    };

    // petition for the service
    let petition: NewUserFormBodyInterface = {
      json: jsonPetition
    };

    console.log('petición: ', JSON.stringify(petition.json));
    try {
      // recording the candidate
      let process = await this.userService.newUser(petition);
      if (process) {
        let message: string = 'Usuario creado correctamente';
        this.sweetAlertService.succesAlert(message);
        setTimeout(() => {
          this.router.navigateByUrl(`/home`);
        }, 1500);
      } else {
        console.log('error línea 153');
      }
    } catch (error) {
      console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlert('No se pudo crear el usuairo, intente más tarde');
    }
  }

  // redirects to route: candidates panel
  cancel() {
    this.router.navigateByUrl(`/home`);
  }




}
