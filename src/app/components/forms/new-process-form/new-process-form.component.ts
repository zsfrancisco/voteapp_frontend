// This component mamages the new process logic

import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { NewProcessFormBodyInterface, JsonProcessBodyInterface, ProcessReturned, EditProcessBodyInterface, EditProcessFormBodyInterface } from '@interfaces';
import { AuthService } from '../../../services/auth.service';
import { ProcessService } from '../../../services/process.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DateService } from '../../../services/date.service';
import moment, { Moment } from 'moment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-process-form',
  templateUrl: './new-process-form.component.html',
  styleUrls: ['./new-process-form.component.css']
})
export class NewProcessFormComponent implements OnInit {

  @Input() isEdit: boolean = false;

  // Form
  form: FormGroup;

  // HTML REFERENCES STARTS
  //  fileinputfile reference
  @ViewChild('fileInputImage') fileInputImage;
  fileToUploadImage: File = null;
  //  fileinputVoters reference
  @ViewChild('fileInputVoters') fileInputVoters;
  // HTML REFERENCES ENDS

  // Excel file references to the voters
  fileToUploadVoters: File = null;

  // Process image src reference
  processImage: string = 'assets/img/default-image.jpg';

  // Default dates
  initTime = { hour: 8, minute: 0 };
  finishTime = { hour: 16, minute: 0 };

  // meridian: allows to the user change the AM and PM options in the hour
  meridian: boolean = true;

  // Flag is loading the submit action
  isLoading: boolean = true;

  // Voters images files
  votersImagesFiles: File[] = [];

  mindate: NgbDateStruct = this.dateService.newDate();

  isProcessLoading: boolean = true;
  isProcessError: boolean = false;

  currentProcess: string = '';

  submitButtonText: string = 'GUARDAR';

  // VOTERS AND CANDIDATES VALIDATIONS STARTS
  votersNotValid: boolean = false;
  votersValid: boolean = false;
  candidatesNotValid: boolean = false;
  candidatesValid: boolean = false;
  votersImagesFilesNotValid: boolean = false;
  // VOTERS AND CANDIDATES VALIDATIONS ENDS

  constructor(
    private formBuilder: FormBuilder,
    private sweetAlertService: SweetAlertService,
    private authService: AuthService,
    private processService: ProcessService,
    private router: Router,
    private dateService: DateService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.mindate.day;
    if (this.isEdit) {
      let processId: string;
      this.activatedRoute.params.subscribe(params => processId = params['processId']);
      this.getProcessImage(processId);
      this.getProcess(processId);
      this.currentProcess = processId;
      this.submitButtonText = 'GUARDAR CAMBIOS';
    } else {
      this.createForm();
    }
  }

  async getProcess(processId: string) {
    try {
      const process = await this.processService.getProcess(processId) as ProcessReturned;
      this.isProcessLoading = true
      this.isProcessError = false;
      const initDate = this.dateService.dateToGet(process.start_date);
      const finishDate = this.dateService.dateToGet(process.end_date);
      const inithour = this.dateService.hourToGet(process.start_date);
      const finishhour = this.dateService.hourToGet(process.end_date);
      // console.log('fecha inicial: ', initDate);
      // console.log('fecha final: ', finishDate);
      this.createForm(
        process.name,
        process.description,
        initDate,
        finishDate,
        inithour,
        finishhour
      );

    } catch (error) {
      this.isProcessError = true;
      this.isProcessLoading = false;
      this.sweetAlertService.errorAlertRedirect(`Ocurrió un error al cargar los datos del proceso electoral: ${processId}`, '/');
    }
  }

  async getProcessImage(processId: string) {
    try {
      this.isProcessLoading = true
      this.isProcessError = false;
      const image = await this.processService.getProcessImage(processId) as string;
      this.processImage = image;
    } catch (error) {
      this.isProcessError = true;
      this.isProcessLoading = false;
      this.sweetAlertService.errorAlertRedirect(`Ocurrió un error al cargar los datos del proceso electoral: ${processId}`, '/');
    }
  }

  // Creates a form
  createForm(
    name = "",
    description = "",
    initDate = this.mindate,
    finishDate = this.mindate,
    initHour = this.initTime,
    finishHour = this.finishTime,
  ) {
    const validator = this.isEdit ? [] : [Validators.required];
    this.form = this.formBuilder.group({
      name: [name, [Validators.required]],
      description: [description, [Validators.required]],
      initDate: [initDate, [Validators.required]],
      finishDate: [finishDate, [Validators.required]],
      initHour: [initHour, [Validators.required]],
      finishHour: [finishHour, [Validators.required]],
      image: ['', validator],
    });
    this.isProcessLoading = false;
  }

  // GETTERS FORM VALIDATIONS STARTS
  get nameNotValid() {
    return this.form.get('name').invalid && this.form.get('name').touched;
  }
  get descriptionNotValid() {
    return this.form.get('description').invalid && this.form.get('description').touched;
  }
  get initDateNotValid() {
    return this.form.get('initDate').invalid && this.form.get('initDate').touched;
  }
  get initHourNotValid() {
    return this.form.get('initHour').invalid && this.form.get('initHour').touched;
  }
  get finishDateNotValid() {
    return this.form.get('finishDate').invalid && this.form.get('finishDate').touched;
  }
  get finishHourNotValid() {
    return this.form.get('finishHour').invalid && this.form.get('finishHour').touched;
  }
  get imageNotValid() {
    return this.form.get('image').invalid && this.form.get('image').touched;
  }
  // GETTERS FORM VALIDATIONS ENDS

  // HANDLE FILES STARTS
  // Function that runs when a file is selected from the template and will be the log image
  handleFileInput(files: FileList) {
    console.log('Se quiere cambiar la imagen');
    
    if (files.item(0).type === 'image/jpg' || files.item(0).type === 'image/jpeg') {
      this.fileToUploadImage = files.item(0);
      let reader = new FileReader;
      reader.onload = (e: any) => {
        this.processImage = e.target.result;
      };
      reader.readAsDataURL(this.fileToUploadImage);
    } else {
      this.processImage = 'assets/img/image-error-1.jpg';
      this.sweetAlertService.errorAlert('Verifique que el archivo sea tipo jpg, jpeg o png y vuelva a intentarlo');
      // console.log(files.item(0).type);
    }
  }
  // File voters
  handleFileVoters(files: FileList) {
    if (files.item(0).type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || files.item(0).type === 'application/vnd.ms-excel') {
      this.fileToUploadVoters = files.item(0);
      this.votersValid = true;
      this.votersNotValid = false;
    } else {
      // console.log('etra al else');
      this.votersValid = false;
      this.votersNotValid = true;
      this.sweetAlertService.errorAlert('Verifique que el archivo sea tipo csv o xlsx y vuelva a intentarlo');
      // console.log(files.item(0).type);
    }
  }
  // User selects an image
  onSelect(event) {
    // console.log(event);
    this.votersImagesFiles.push(...event.addedFiles);
  }
  // User removes an image
  onRemove(event) {
    // console.log(event);
    this.votersImagesFiles.splice(this.votersImagesFiles.indexOf(event), 1);
  }
  // HANDLE FILES ENDS

  // Form subit
  async onSubmit() {
    // Is form is invalid, returns the errors in the fields
    if (!this.isEdit) {
      console.log('Va a crear');
      if (this.form.invalid || !this.fileToUploadImage || !this.fileToUploadVoters || this.votersImagesFiles.length < 1) {
        if (!this.fileToUploadImage) {
          this.processImage = 'assets/img/image-error-1.jpg';
        }
        !this.fileToUploadVoters ? this.votersNotValid = true : this.votersNotValid = false;
        this.votersImagesFiles.length <= 0 ? this.votersImagesFilesNotValid = true : this.votersImagesFilesNotValid = false;
        return Object.values(this.form.controls).forEach(control => {
          control.markAsTouched();
        });
      }
    } else {
      console.log('Va a editar');
      console.log('name status: ', this.form.controls.name.invalid);
      console.log('description status: ', this.form.controls.description.invalid);
      console.log('initDate status: ', this.form.controls.initDate.invalid);
      console.log('finishDate status: ', this.form.controls.finishDate.invalid);
      console.log('initHour status: ', this.form.controls.initHour.invalid);
      console.log('finishHour status: ', this.form.controls.finishHour.invalid);
      if (this.form.invalid) {
        console.log('Formulario inválido');
        return Object.values(this.form.controls).forEach(control => {
          control.markAsTouched();
        });
      }
    }

    // Message to the user
    const message = this.isEdit ? "Modificando registro del proceso de votación" : "Registrando nuevo proceso de votación";
    this.sweetAlertService.loadingAlert(message);

    // petitions variables
    let startDate: NgbDateStruct = this.form.controls.initDate.value;
    let starttime = this.form.controls.initHour.value;
    let finishDate: NgbDateStruct = this.form.controls.finishDate.value;
    let finishtime = this.form.controls.finishHour.value;
    // let startDateForm: string = `${startDate.year}-${startDate.month}-${startDate.day} ${starttime.hour}:${starttime.minute}:00`;
    // let finishDateForm: string = `${finishDate.year}-${finishDate.month}-${finishDate.day} ${finishtime.hour}:${finishtime.minute}:00`;

    const startDateForm = moment(`${startDate.year}-${startDate.month}-${startDate.day} ${starttime.hour}:${starttime.minute}:00`).format('YYYY-MM-DD HH:mm:ss')
    const finishDateForm = moment(`${finishDate.year}-${finishDate.month}-${finishDate.day} ${finishtime.hour}:${finishtime.minute}:00`).format('YYYY-MM-DD HH:mm:ss')

    if (startDateForm > finishDateForm || startDateForm < this.dateService.hour()) {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'Fechas incorrectas',
      });
      return;
    }

    !this.isEdit ? this.createProcess(startDateForm, finishDateForm) : this.editProcess(startDateForm, finishDateForm);

  }

  async createProcess(startDateForm: string, finishDateForm: string) {
    // Defining json Petition to the service petition
    let jsonPetition: JsonProcessBodyInterface = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      start_date: startDateForm,
      end_date: finishDateForm,
      user_id: this.authService.readUserId(),
    }

    // Defining petition to the service
    let petition: NewProcessFormBodyInterface = {
      'file_users': this.fileToUploadVoters,
      'json': jsonPetition,
      'file0': this.fileToUploadImage,
    }

    // console.log('petición: ', petition);
    // console.log('formulario: ', this.form.value);
    try {
      // Recording the process in database
      
      // Recording the voters images
      for (const file in this.votersImagesFiles) {
        try {
          // console.log('Archivo: ', this.files[file]);
          // Recording a image in the server
          const answer = await this.processService.uploadVoterImage(this.votersImagesFiles[file]);
        } catch (error) {
          // console.warn('No se subió la imagen: Error - ', error);
          this.sweetAlertService.errorAlertRedirect('No se pudo cargar las imágenes de los votantes, intente más tarde', '/process/panel');
        }
      }
      
      await this.processService.newProcess(petition);
      
      let message: string = 'Proceso creado satisfactoriamente';
      this.sweetAlertService.succesAlert(message);

      setTimeout(() => {
        this.router.navigateByUrl('/process/panel');
      }, 1500);

    } catch (error) {
      // console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlertRedirect('No se pudo crear el proceso de votación, intente más tarde', '/process/panel');
    }
  }

  async editProcess(startDateForm: string, finishDateForm: string) {
    // Defining json Petition to the service petition
    let jsonPetition: EditProcessBodyInterface = {
      name: this.form.controls.name.value,
      description: this.form.controls.description.value,
      start_date: startDateForm,
      end_date: finishDateForm,
      user_id: this.authService.readUserId(),
      status: '0',
    }

    // Defining petition to the service
    let petition: EditProcessFormBodyInterface = {
      'file_users': this.fileToUploadVoters,
      'json': jsonPetition,
      'file0': this.fileToUploadImage,
    }

    // console.log('petición: ', petition);
    // console.log('formulario: ', this.form.value);
    try {
      // Recording the process in database
      
      // Recording the voters images
      if (this.votersImagesFiles.length > 0) {
        for (const file in this.votersImagesFiles) {
          try {
            // console.log('Archivo: ', this.files[file]);
            // Recording a image in the server
            const answer = await this.processService.uploadVoterImage(this.votersImagesFiles[file]);
          } catch (error) {
            // console.warn('No se subió la imagen: Error - ', error);
            this.sweetAlertService.errorAlertRedirect('No se pudo cargar las imágenes de los votantes, intente más tarde', '/process/panel');
          }
        }
      }
      await this.processService.editProcess(petition, this.currentProcess);
      
      let message: string = 'Proceso modificado satisfactoriamente';
      this.sweetAlertService.succesAlert(message);

      setTimeout(() => {
        this.router.navigateByUrl('/process/panel');
      }, 1500);

    } catch (error) {
      console.log('entra al catch: ', error);
      this.sweetAlertService.errorAlertRedirect('No se pudo modificar el proceso de votación, intente más tarde', '/process/panel');
    }
  }

}