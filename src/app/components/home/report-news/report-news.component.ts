// this component controls the logic for redirect to new report news form

import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { VoterRequestService } from '../../../services/voter-request.service';
import { CandidateRequestReturned, IVoterRequest } from '@interfaces';
import moment from 'moment';
import { CandidateRequestService } from '../../../services/candidate-request.service';

@Component({
  selector: 'app-report-news',
  templateUrl: './report-news.component.html',
  styleUrls: ['./report-news.component.css']
})
export class ReportNewsComponent implements OnInit {

  @Input() isCandidate = false;

  textButton = "Solicitar revisión";

  constructor(
    private router: Router,
    private sweetAlertService: SweetAlertService,
    private voterRequestService: VoterRequestService,
    private candidateRequestService: CandidateRequestService,
  ) { }

  ngOnInit(): void {
    if (this.isCandidate) this.textButton = "Realizar solicitud";
  }

  // reportNews redirects to the new report news route
  reportNews() {
    this.isCandidate ?
      this.router.navigateByUrl('/candidate/report-news') :
      this.router.navigateByUrl('/voter/report-news');
  }

  // Alert and process for consultof a voter
  async consultState() {
    const { value: numberRequest } = await Swal.fire({
      title: '<span class="font-028090">Consultar el estado de una solicitud<span/>',
      text: '<span class="font-028090">Por favor, ingrese su número de radicado<span/>',
      html:
        `
        <input id="userNumber" 
              class="swal2-input"
              placeholder="Número de radicado"
              required>
        `,
      confirmButtonText: "Consultar",
      confirmButtonColor: '#05668d',
      focusConfirm: false,
      preConfirm: () => {
        if ((document.getElementById('userNumber') as HTMLInputElement).value != "") {
          return (document.getElementById('userNumber') as HTMLInputElement).value;
        } else {
          Swal.showValidationMessage('El número de radicado es obligatorio');
        }
      }
    });

    // Pushing the proposal
    if (numberRequest) {
      this.sweetAlertService.loadingAlert('Consultando');
      console.log('imprimiendo identificación del cudadano: ', numberRequest);
      try {
        const request = await this.voterRequestService.getVoterRequest(numberRequest) as IVoterRequest;
        let requestStatus: string;
        switch (request.status) {
          case '0':
            requestStatus = 'En revisión';
            break;
          case '1':
            requestStatus = 'En proceso';
            break;
          case '2':
            requestStatus = 'Revisado';
            break;

          default:
            requestStatus = 'En revisión';
            break;
        }
        // console.log("Tipo de comentario: ", typeof (request.comments));
        // console.log("igual a null: ", request.reply == null);
        const reply = request.reply != null ? request.reply : "";
        const initDate = this.date(request.created_at);
        const updateDate = this.date(request.updated_at);
        Swal.fire({
          title: '<span class="font-028090">Detalles de la Solicitud<span/>',
          html:
            `
        <div class="container text-left">
          <span class="font-05668D font-weight-bold">ID:</span> <span>${request.id}</span> <br>
          <small class="font-05668D font-weight-bold">Datos del votante</small> <br>
          <span class="font-05668D font-weight-bold">Identificación:</span> <span>${request.identification}</span> <br>
          <span class="font-05668D font-weight-bold">Nombre completo:</span> <span>${request.first_name} ${request.last_name}</span> <br>
          <span class="font-05668D font-weight-bold">Correo:</span> <span>${request.email}</span> <br>
          <small class="font-05668D font-weight-bold">Datos de la solicitud</small> <br>
          <span class="font-05668D font-weight-bold">Proceso de votación:</span> <span>${request.voting_process_name} ${request.voting_process_id}</span> <br>
          <span class="font-05668D font-weight-bold">Comentarios:</span> <span>${request.comments}</span> <br>
          <span class="font-05668D font-weight-bold">Estado:</span> <span>${requestStatus}</span> <br>
          <span class="font-05668D font-weight-bold">Respuesta:</span> <span>${reply}</span> <br>
          <span class="font-05668D font-weight-bold">Fecha de creación:</span> <span>${initDate}</span> <br>
          <span class="font-05668D font-weight-bold">Última modificacion:</span> <span>${updateDate}</span> <br>
        </div>
        `,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
          confirmButtonColor: '#05668d',
        });
      } catch (error) {
        this.sweetAlertService.errorAlert(error.error.message);
      }
    }
  }

  // Alert and process for consult of a candidate
  async consultCandidateState() {
    const { value: numberRequest } = await Swal.fire({
      title: '<span class="font-028090">Consultar el estado de una solicitud<span/>',
      text: '<span class="font-028090">Por favor, ingrese su número de radicado<span/>',
      html:
        `
        <input id="userNumber" 
              class="swal2-input"
              placeholder="Número de radicado"
              required>
        `,
      confirmButtonText: "Consultar",
      confirmButtonColor: '#05668d',
      focusConfirm: false,
      preConfirm: () => {
        if ((document.getElementById('userNumber') as HTMLInputElement).value != "") {
          return (document.getElementById('userNumber') as HTMLInputElement).value;
        } else {
          Swal.showValidationMessage('El número de radicado es obligatorio');
        }
      }
    });

    // Pushing the proposal
    if (numberRequest) {
      this.sweetAlertService.loadingAlert('Consultando');
      console.log('imprimiendo identificación del candidato: ', numberRequest);
      try {
        const request = await this.candidateRequestService.getCandidateRequest(numberRequest) as CandidateRequestReturned;
        console.log('Solicitud candidato: ', request);
        let requestStatus: string;
        switch (request.status) {
          case '0':
            requestStatus = 'En revisión';
            break;
          case '1':
            requestStatus = 'En proceso';
            break;
          case '2':
            requestStatus = 'Revisado';
            break;

          default:
            requestStatus = 'En revisión';
            break;
        }
        // console.log("Tipo de comentario: ", typeof (request.comments));
        // console.log("igual a null: ", request.reply == null);
        const reply = request.reply != null ? request.reply : "";
        const initDate = this.date(request.created_at);
        const updateDate = this.date(request.updated_at);
        Swal.fire({
          title: '<span class="font-028090">Detalles de la Solicitud<span/>',
          html:
            `
        <div class="container text-left">
          <span class="font-05668D font-weight-bold">ID:</span> <span>${request.id}</span> <br>
          <small class="font-05668D font-weight-bold">Datos de la solicitud</small> <br>
          <span class="font-05668D font-weight-bold">Proceso de votación:</span> <span>${request.voting_process_name} ${request.voting_process_id}</span> <br>
          <span class="font-05668D font-weight-bold">Comentarios:</span> <span>${request.comments}</span> <br>
          <span class="font-05668D font-weight-bold">Estado:</span> <span>${requestStatus}</span> <br>
          <span class="font-05668D font-weight-bold">Respuesta:</span> <span>${reply}</span> <br>
          <span class="font-05668D font-weight-bold">Fecha de creación:</span> <span>${initDate}</span> <br>
          <span class="font-05668D font-weight-bold">Última modificacion:</span> <span>${updateDate}</span> <br>
        </div>
        `,
          confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
          confirmButtonColor: '#05668d',
        });
      } catch (error) {
        this.sweetAlertService.errorAlert(error);
      }
    }
  }

  date(date: string) {
    const dateProv = new Date(date);
    return moment(dateProv, "MM-DD-YYYY").format("MM-DD-YYYY HH:mm:ss");
  }

}
