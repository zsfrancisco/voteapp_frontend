import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { ProcessService } from 'src/app/services/process.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-check-certificate',
  templateUrl: './check-certificate.component.html',
  styleUrls: ['./check-certificate.component.css']
})
export class CheckCertificateComponent implements OnInit {

  constructor(
    private router: Router,
    private sweetAlertService: SweetAlertService,
    private processService: ProcessService,
  ) { }

  ngOnInit(): void {
  }

  async checkCertificate() {
    const { value: userNumberCertificate } = await Swal.fire({
      title: '<span class="font-028090">Consultar la validez de mi certificado<span/>',
      text: '<span class="font-028090">Por favor, ingrese el número que le devuelve el código QR de su certificado.<span/>',
      html:
        `
        <input id="certificateNumber" 
              class="swal2-input"
              placeholder="Número del certificado"
              required>
        `,
      confirmButtonText: "Consultar",
      confirmButtonColor: '#05668d',
      focusConfirm: false,
      preConfirm: () => {
        if ((document.getElementById('certificateNumber') as HTMLInputElement).value != "") {
          return (document.getElementById('certificateNumber') as HTMLInputElement).value;
        } else {
          Swal.showValidationMessage('El número del certificado es obligatorio');
        }
      }
    });

    // Pushing the proposal
    if (userNumberCertificate) {
      this.sweetAlertService.loadingAlert('Consultando');
      console.log('imprimiendo identificación del cudadano: ', userNumberCertificate);
      try {
        const answer: any = await this.processService.checkCertificate(userNumberCertificate);
        // console.log('Si encontró procesos para votar: ', processes);
        // This function shows  an error alert
        Swal.fire({
          icon: 'success',
          title: '¡Éxito!',
          text: 'El certificado es válido',
          html: `
            <div class="text-left">
              <small class="font-05668D">Datos del proceso</small>
              <p>Nombre: ${ answer.voting_process }</p>
              <small class="font-05668D">Datos del votante</small>
              <p>Identificación: ${ answer.identification }</p>
              <p>Nombre: ${ answer.first_name } ${ answer.last_name }</p>
              <p>Fecha: ${ answer.vote_date }</p>
              <p>Número de certificado ${ answer.certificateNumber }</p>
            </div>
          `,
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          showConfirmButton: true,
          confirmButtonColor: '#05668d',
          confirmButtonText: '<span class="span-alert-error">Aceptar</span>',
        });
      } catch (error) {
        // console.log('error haciendo eso: ', error);
        this.sweetAlertService.errorAlert(error.error.message);
      }
    }
  }

  // Alert and process for consult
  async consultSuitable() {
    const { value: userIdentification } = await Swal.fire({
      title: '<span class="font-028090">Consultar si soy apto para votar<span/>',
      text: '<span class="font-028090">Por favor, ingrese su identificación<span/>',
      html:
        `
        <input id="userIdentificationId" 
              class="swal2-input"
              placeholder="Identificación de ciudadano"
              required>
        `,
      confirmButtonText: "Consultar",
      confirmButtonColor: '#05668d',
      focusConfirm: false,
      preConfirm: () => {
        if ((document.getElementById('userIdentificationId') as HTMLInputElement).value != "") {
          return (document.getElementById('userIdentificationId') as HTMLInputElement).value;
        } else {
          Swal.showValidationMessage('La identificación del ciudadano es obligatoria');
        }
      }
    });

    // Pushing the proposal
    if (userIdentification) {
      this.sweetAlertService.loadingAlert('Consultando');
      console.log('imprimiendo identificación del cudadano: ', userIdentification);
      try {
        const processes = await this.processService.consultSuitable(userIdentification);
        // console.log('Si encontró procesos para votar: ', processes);
        Swal.close();
        this.router.navigateByUrl(`/voter/${userIdentification}/process`);
      } catch (error) {
        this.sweetAlertService.errorAlert(error.error.message);
      }
    }
  }

}
