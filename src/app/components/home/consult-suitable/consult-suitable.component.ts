// This component controls the logic of consult if a user is allowed for vote in one o more electoral processes

import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { ProcessService } from '../../../services/process.service';

@Component({
  selector: 'app-consult-suitable',
  templateUrl: './consult-suitable.component.html',
  styleUrls: ['./consult-suitable.component.css']
})
export class ConsultSuitableComponent implements OnInit {

  constructor(
    private router: Router,
    private sweetAlertService: SweetAlertService,
    private processService: ProcessService,
  ) { }

  ngOnInit(): void {
  }

  // Alert and process for consult
  async consultSuitable() {
    const { value: userIdentification } = await Swal.fire({
      title: '<span class="font-028090">Consultar si soy apto para votar<span/>',
      text: '<span class="font-028090">Por favor, ingrese su identificación<span/>',
      html:
        `
        <input id="userIdentificationId" 
              class="swal2-input"
              placeholder="Identificación de ciudadano"
              required>
        `,
      confirmButtonText: "Consultar",
      confirmButtonColor: '#05668d',
      focusConfirm: false,
      preConfirm: () => {
        if ((document.getElementById('userIdentificationId') as HTMLInputElement).value != "" ){
          return (document.getElementById('userIdentificationId') as HTMLInputElement).value;
        } else {
          Swal.showValidationMessage('La identificación del ciudadano es obligatoria');
        }
      }
    });

    // Pushing the proposal
    if (userIdentification) {
      this.sweetAlertService.loadingAlert('Consultando');
      console.log('imprimiendo identificación del cudadano: ', userIdentification);
      try {
        const processes = await this.processService.consultSuitable(userIdentification);
        // console.log('Si encontró procesos para votar: ', processes);
        Swal.close();
        this.router.navigateByUrl(`/voter/${userIdentification}/process`);
      } catch (error) {
        this.sweetAlertService.errorAlert(error.error.message);
      }
    }
  }

}
