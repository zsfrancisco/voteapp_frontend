import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProcessReturned, ProcessCardTemplateInterface } from '@interfaces';
import { ProcessService } from 'src/app/services/process.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-grid-process',
  templateUrl: './grid-process.component.html',
  styleUrls: ['./grid-process.component.css']
})
export class GridProcessComponent implements OnInit {

  // Form
  form: FormGroup;

  // Process
  processesArray: ProcessCardTemplateInterface[] = [];
  processesArrayWhenIsSearched: ProcessCardTemplateInterface[] = [];

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;
  isSearch: boolean = false;
  isSearchIsEmpty: boolean = false;

  constructor(
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
  ) { }

  executeInterval = 0;

  ngOnInit(): void {
    this.loadProcess();
    localStorage.setItem("executeInterval", "10");
    setInterval(() => {
      let value = localStorage.getItem("executeInterval");
      if (+value === this.executeInterval) {
        this.loadProcess();
        localStorage.setItem("executeInterval", this.executeInterval + 10 + "");
      } else {
        this.executeInterval++;
      }
    }, 1000);
  }

  // loadProcess fill the process array for showed in the template
  async loadProcess() {
    this.isLoading = true;
    this.isError = false;
    this.isEmpty = false;
    this.processesArray = [];
    try {
      let processprov: ProcessReturned[] = await this.processService.getProcesses() as ProcessReturned[];
      for (let index = 0; index < processprov.length; index++) {
        if (processprov[index].status == '0' || processprov[index].status == '1') {
          console.log('Entra al for normal');
          const processImage = await this.processService.getProcessImage(processprov[index].id);
          const proccessCard: ProcessCardTemplateInterface = {
            id: processprov[index].id,
            name: processprov[index].name,
            description: processprov[index].description,
            start_date: processprov[index].start_date,
            image: processImage,
            status: processprov[index].status,
          }
          this.processesArray.push(proccessCard);
        }
      }
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlert('Ocurrió un error cargando los registros, inténtelo más tarde');
    }
    if (this.processesArray.length > 0) {
      this.isEmpty = false;
    } else this.isEmpty = true;
    console.log('Procesos retornados: ', this.processes);
    this.isLoading = false
    // console.log(this.processes);
  }

  // processes returns the data with the pagination
  get processes(): ProcessCardTemplateInterface[] {
    return this.processesArray;
  }

  // searchProcess searchs a processes by name
  searchProcess(text: string) {
    console.log(`El valor del texto: ${text}`);
    text = text.toLowerCase();
    if (text === '') {
      this.isSearch = false;
    } else {
      this.processesArrayWhenIsSearched = [];
      this.isSearch = true;
      this.processes.forEach(process => {
        let processName = process.name.toLowerCase();
        if (processName.indexOf(text) >= 0) this.processesArrayWhenIsSearched.push(process);
      });
      this.processesArrayWhenIsSearched.length <= 0 ? this.isSearchIsEmpty = true : this.isSearchIsEmpty = false;
    }
  }

  viewCandidates(processId: string) {
    this.router.navigateByUrl(`process/${processId}/candidates`);
  }

  goAccess(processId: string) {
    document.location.href = `http://localhost/evoting/public/authentication/${processId}`;
  }

}
