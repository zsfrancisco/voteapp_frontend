import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridProcessComponent } from './grid-process.component';

describe('GridProcessComponent', () => {
  let component: GridProcessComponent;
  let fixture: ComponentFixture<GridProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
