import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginAdminComponent } from './pages/admin/login-admin/login-admin.component';
import { AccessGuard } from './guards/access.guard';
import { AdminGuard } from './guards/admin.guard';
import { NewProcessComponent } from './pages/process/new-process/new-process.component';
import { PanelProcessComponent } from './pages/process/panel-process/panel-process.component';
import { NewCandidateComponent } from './pages/candidates/new-candidate/new-candidate.component';
import { PanelCandidatesComponent } from './pages/candidates/panel-candidates/panel-candidates.component';
import { HomeComponent } from './pages/home/home.component';
import { ProcessesPermittedComponent } from './pages/voters/processes-permitted/processes-permitted.component';
import { VoterReportNewsComponent } from './pages/voters/voter-report-news/voter-report-news.component';
import { AboutComponent } from './pages/about/about.component';
import { VoteGridComponent } from './pages/process/vote-grid/vote-grid.component';
import { CandidatesProcessComponent } from './pages/process/candidates-process/candidates-process.component';
import { AccessVoteProcessComponent } from './pages/process/access-vote-process/access-vote-process.component';
import { VoteAccessGuard } from './guards/vote-access.guard';
import { VoterGuard } from './guards/voter.guard';
import { ResultProcessComponent } from './pages/process/result-process/result-process.component';
import { ErrorAccessComponent } from './pages/process/error-access/error-access.component';
import { EditCandidateComponent } from './pages/candidates/edit-candidate/edit-candidate.component';
import { EditProcessComponent } from './pages/process/edit-process/edit-process.component';
import { ResultGraphicsProcessComponent } from './pages/process/result-graphics-process/result-graphics-process.component';
import { PanelProposalsComponent } from './pages/candidates/panel-proposals/panel-proposals.component';
import { PanelAuthLogComponent } from './pages/process/panel-auth-log/panel-auth-log.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { CreateUserComponent } from './pages/users/create-user/create-user.component';
import { HomeCandidateComponent } from './pages/candidates/home-candidate/home-candidate.component';
import { CandidateGuard } from './guards/candidate.guard';
import { CandidateReportNewsComponent } from './pages/candidates/candidate-report-news/candidate-report-news.component';


//   { path: 'ally/modify/:id', component: ModifyAllyComponent,   canActivate: [ AuthGuard ] },

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  // Login administrator
  { path: 'login', component: LoginAdminComponent, canActivate: [ AccessGuard ] },
  // Process pages
  { path: 'process/panel', component: PanelProcessComponent, canActivate: [AdminGuard] },
  { path: 'process/new_process', component: NewProcessComponent, canActivate: [AdminGuard] },
  { path: 'process/edit_process/:processId', component: EditProcessComponent, canActivate: [AdminGuard] },
  { path: 'process/access/error/:errorId', component: ErrorAccessComponent },
  { path: 'process/results/:processId', component: ResultGraphicsProcessComponent },
  { path: 'process/:processId/candidates', component: CandidatesProcessComponent },
  { path: 'process/:processId/auth_logs', component: PanelAuthLogComponent, canActivate: [AdminGuard] },
  { path: 'process/:processId/access/vote', component: AccessVoteProcessComponent, canActivate: [VoteAccessGuard] },
  { path: 'process/:processId/:voterId/vote', component: VoteGridComponent, canActivate: [VoterGuard] },
  { path: 'process/results', component: ResultProcessComponent },
  { path: 'process/requests', component: RequestsComponent, canActivate: [AdminGuard] },
  // Candidates pages
  { path: 'process/:processId/candidates-panel', component: PanelCandidatesComponent, canActivate: [AdminGuard] },
  { path: 'process/:processId/candidates-panel/:candidateId/proposals', component: PanelProposalsComponent },
  { path: 'process/:processId/candidates/new_candidate', component: NewCandidateComponent, canActivate: [AdminGuard] },
  { path: 'process/:processId/candidates/edit_candidate/:candidateId', component: EditCandidateComponent, canActivate: [AdminGuard] },
  // Voters pages
  { path: 'voter/:voterId/process', component: ProcessesPermittedComponent },
  { path: 'voter/report-news', component: VoterReportNewsComponent },
  // users pages 
  { path: 'user/create', component: CreateUserComponent, canActivate: [AdminGuard] },
  // Candidate pages
  { path: 'candidate/home', component: HomeCandidateComponent, canActivate: [ CandidateGuard ] },
  { path: 'candidate/report-news', component: CandidateReportNewsComponent, canActivate: [ CandidateGuard ] },
  // Default page
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
