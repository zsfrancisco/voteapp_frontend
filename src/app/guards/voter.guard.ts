// This guard controls access to the identification form page pf the one process vote access

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class VoterGuard implements CanActivate {
  constructor(
     private authService: AuthService,
     private router: Router 
     ) {}
  canActivate(): boolean {
    // if ( this.authService.isVoter() ) {
    //     return true;
    // } else {
    //     this.router.navigateByUrl('/');
    //     return true;
    // }
    return true;
  }
}