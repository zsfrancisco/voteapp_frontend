// This guard controls access to the identification form page pf the one process vote access

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class VoteAccessGuard implements CanActivate {
  constructor(
     private authService: AuthService,
     private router: Router 
     ) {}
  canActivate(): boolean {
    if ( this.authService.isVoter() ) {
        const currentProcess = this.authService.readCurrentProcess();
        console.log('Donde me debe enviar el guard: ', `/process/${currentProcess}/vote`);
        this.router.navigateByUrl(`/process/${currentProcess}/vote`);
        return false;
    } else {
        return true;
    }
  }
}