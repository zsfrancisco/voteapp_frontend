// This guard controls access to the registration and login routes

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {
  constructor(
     private authService: AuthService,
     private router: Router 
     ) {}
  canActivate(): boolean {
    if ( this.authService.isAuth() ) {
        this.authService.readRoleId() == "1" ? 
        this.router.navigateByUrl('/process/panel') :
        this.router.navigateByUrl('/candidate/home');
        return false;
    } else {
        return true;
    }
  }
}