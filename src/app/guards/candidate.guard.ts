// This guard controls access to the candidate pages

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class CandidateGuard implements CanActivate {
  constructor( 
    private authService: AuthService,
    private router: Router 
    ) {}
  canActivate(): boolean {
    if ( this.authService.isAuth() && this.authService.readRoleId() == "2" ) {
      return true;
    }
    else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
