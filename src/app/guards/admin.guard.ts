// This guard controls access to the administrator pages

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor( 
    private authService: AuthService,
    private router: Router 
    ) {}
  canActivate(): boolean {
    if ( this.authService.isAuth() && this.authService.readRoleId() == "1" ) {
      return true;
    }
    else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
