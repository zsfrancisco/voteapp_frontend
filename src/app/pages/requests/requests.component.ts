import { Component, OnInit } from '@angular/core';
import { VoterRequestService } from '../../services/voter-request.service';
import { IVoterRequest, IJsonChangeVoterRequest, IChangeVoterRequest, CandidateRequestReturned, JsonChangeCandidateRequest } from '@interfaces';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import moment from 'moment';
import { CandidateRequestService } from '../../services/candidate-request.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {

  private _request: IVoterRequest[] = [];
  private _candidatesRequest: CandidateRequestReturned[] = [];

  // controls the pagination
  page = 1;
  pageSize = 4;
  collectionSize = this._request.length;

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;
  isCandidateEmpty: boolean = false;

  // Options in alert for radioButtons
  inputOptions = {
    '0': 'Sin revisión',
    '1': 'En proceso',
    '2': 'Revisado',
  }

  isCandidate = false;

  constructor(
    private voterRequestService: VoterRequestService,
    private candidateRequestService: CandidateRequestService,
    private sweetAlertService: SweetAlertService,
  ) { }

  async ngOnInit() {
    await this.getVotersRequests();
    await this.getCandidatesRequests();
  }

  async getVotersRequests() {
    try {
      this._request = [];
      this._request = await this.voterRequestService.getVotersRequest() as IVoterRequest[];
      console.log('solicitudes: ', this._request);
      this.collectionSize = this._request.length;
      this._request.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
    } catch (error) {
      console.log('Error: ', error);
      this.isError = true;
    }
    this.isLoading = false;
  }

  // processes returns the data with the pagination
  get voterRequests(): IVoterRequest[] {
    return this._request
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  async getCandidatesRequests() {
    try {
      this._candidatesRequest = [];
      this._candidatesRequest = await this.candidateRequestService.getCandidatesRequest() as CandidateRequestReturned[];
      console.log('solicitudes: ', this._candidatesRequest);
      this.collectionSize = this._candidatesRequest.length;
      this._candidatesRequest.length <= 0 ? this.isCandidateEmpty = true : this.isCandidateEmpty = false;
    } catch (error) {
      console.log('Error: ', error);
      this.isError = true;
    }
    this.isLoading = false;
  }

  // processes returns the data with the pagination
  get candidatesRequests(): CandidateRequestReturned[] {
    return this._candidatesRequest
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  // viewDetails shows details of a specific request
  viewDetails(request: IVoterRequest) {
    let requestStatus: string;
    switch (request.status) {
      case '0':
        requestStatus = 'En revisión';
        break;
      case '1':
        requestStatus = 'En proceso';
        break;
      case '2':
        requestStatus = 'Revisado';
        break;

      default:
        requestStatus = 'En revisión';
        break;
    }
    // console.log("Tipo de comentario: ", typeof (request.comments));
    // console.log("igual a null: ", request.reply == null);
    const reply = request.reply != null ? request.reply : "";
    const initDate = this.date(request.created_at);
    const updateDate = this.date(request.created_at);
    Swal.fire({
      title: '<span class="font-028090">Detalles de la Solicitud<span/>',
      html:
        `
        <div class="container text-left">
          <span class="font-05668D font-weight-bold">ID:</span> <span>${request.id}</span> <br>
          <small class="font-05668D font-weight-bold">Datos del votante</small> <br>
          <span class="font-05668D font-weight-bold">Identificación:</span> <span>${request.identification}</span> <br>
          <span class="font-05668D font-weight-bold">Nombre completo:</span> <span>${request.first_name} ${request.last_name}</span> <br>
          <span class="font-05668D font-weight-bold">Correo:</span> <span>${request.email}</span> <br>
          <small class="font-05668D font-weight-bold">Datos de la solicitud</small> <br>
          <span class="font-05668D font-weight-bold">Proceso de votación:</span> <span>${request.voting_process_name} ${request.voting_process_id}</span> <br>
          <span class="font-05668D font-weight-bold">Comentarios:</span> <span>${request.comments}</span> <br>
          <span class="font-05668D font-weight-bold">Estado:</span> <span>${requestStatus}</span> <br>
          <span class="font-05668D font-weight-bold">Respuesta:</span> <span>${reply}</span> <br>
          <span class="font-05668D font-weight-bold">Fecha de creación:</span> <span>${initDate}</span> <br>
          <span class="font-05668D font-weight-bold">Última modificacion:</span> <span>${updateDate}</span> <br>
        </div>
        `,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
      confirmButtonColor: '#05668d',
    });
  }

  async onChangeRequest(request: IVoterRequest) {
    const { value: radioStatus } = await Swal.fire({
      title: '<span class="par251a68">Seleccione estado de la solicitud</span>',
      input: 'radio',
      inputOptions: this.inputOptions,
      inputValue: request.status,
      confirmButtonColor: "#05668D",
      confirmButtonText: 'Siguiente',
      inputValidator: (value) => {
        if (!value) {
          return 'Debes seleccionar un estado'
        }
      },
    });
    // Si hay estado seleccionado:
    if (radioStatus) {
      const { value: text } = await Swal.fire({
        title: '<span class="par251a68">Observaciones</span>',
        input: 'textarea',
        inputPlaceholder: request.comments,
        inputAttributes: {
          'aria-label': 'Type your message here'
        },
        showCancelButton: true,
        confirmButtonText: 'Guardar',
        cancelButtonText: '<span class="font-05668D">Cancelar</span>',
        confirmButtonColor: "#05668D",
        cancelButtonColor: "#ffffff",
        reverseButtons: true
      });
      const json: IJsonChangeVoterRequest = {
        voter_request_id: request.id,
        status: radioStatus,
        reply: text,
      }
      this.sweetAlertService.loadingAlert("Actualizando solicitud");
      try {
        const answer = await this.voterRequestService.changeVoterRequest(json) as string;
        this.sweetAlertService.succesAlert(answer);
        setTimeout(() => {
          this.getVotersRequests();
        }, 1500);
      } catch (error) {
        this.sweetAlertService.errorAlert(error.error.message);
      }
    }
  }

  // viewDetails shows details of a specific request
  viewCandidateReqDetails(request: CandidateRequestReturned) {
    let requestStatus: string;
    switch (request.status) {
      case '0':
        requestStatus = 'En revisión';
        break;
      case '1':
        requestStatus = 'En proceso';
        break;
      case '2':
        requestStatus = 'Revisado';
        break;

      default:
        requestStatus = 'En revisión';
        break;
    }
    // console.log("Tipo de comentario: ", typeof (request.comments));
    // console.log("igual a null: ", request.reply == null);
    const reply = request.reply != null ? request.reply : "";
    const initDate = this.date(request.created_at);
    const updateDate = this.date(request.created_at);
    Swal.fire({
      title: '<span class="font-028090">Detalles de la Solicitud<span/>',
      html:
        `
        <div class="container text-left">
          <span class="font-05668D font-weight-bold">ID:</span> <span>${request.id}</span> <br>
          <small class="font-05668D font-weight-bold">Datos del Candidato</small> <br>
          <span class="font-05668D font-weight-bold">Identificador:</span> <span>${request.candidate_id}</span> <br>
          <small class="font-05668D font-weight-bold">Datos de la solicitud</small> <br>
          <span class="font-05668D font-weight-bold">Proceso de votación:</span> <span>${request.voting_process_name} ${request.voting_process_id}</span> <br>
          <span class="font-05668D font-weight-bold">Comentarios:</span> <span>${request.comments}</span> <br>
          <span class="font-05668D font-weight-bold">Estado:</span> <span>${requestStatus}</span> <br>
          <span class="font-05668D font-weight-bold">Respuesta:</span> <span>${reply}</span> <br>
          <span class="font-05668D font-weight-bold">Fecha de creación:</span> <span>${initDate}</span> <br>
          <span class="font-05668D font-weight-bold">Última modificacion:</span> <span>${updateDate}</span> <br>
        </div>
        `,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
      confirmButtonColor: '#05668d',
    });
  }

  async onChangeCandidateRequest(request: CandidateRequestReturned) {
    const { value: radioStatus } = await Swal.fire({
      title: '<span class="par251a68">Seleccione estado de la solicitud</span>',
      input: 'radio',
      inputOptions: this.inputOptions,
      inputValue: request.status,
      confirmButtonColor: "#05668D",
      confirmButtonText: 'Siguiente',
      inputValidator: (value) => {
        if (!value) {
          return 'Debes seleccionar un estado'
        }
      },
    });
    // Si hay estado seleccionado:
    if (radioStatus) {
      const { value: text } = await Swal.fire({
        title: '<span class="par251a68">Observaciones</span>',
        input: 'textarea',
        inputPlaceholder: request.comments,
        inputAttributes: {
          'aria-label': 'Type your message here'
        },
        showCancelButton: true,
        confirmButtonText: 'Guardar',
        cancelButtonText: '<span class="font-05668D">Cancelar</span>',
        confirmButtonColor: "#05668D",
        cancelButtonColor: "#ffffff",
        reverseButtons: true
      });
      const json: JsonChangeCandidateRequest = {
        candidate_request_id: request.id,
        status: radioStatus,
        reply: text,
      }
      this.sweetAlertService.loadingAlert("Actualizando solicitud");
      try {
        const answer = await this.candidateRequestService.changeCandidateRequest(json) as string;
        this.sweetAlertService.succesAlert(answer);
        setTimeout(() => {
          this.getCandidatesRequests();
        }, 1500);
      } catch (error) {
        this.sweetAlertService.errorAlert(error.error.message);
      }
    }
  }

  date(date: string) {
    const dateProv = new Date(date);
    return moment(dateProv, "MM-DD-YYYY").format("MM-DD-YYYY HH:mm:ss");
  }

  onChange(event) {
    this.isCandidate = event == "1" ? false : true;
  }

}
