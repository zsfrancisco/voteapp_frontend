// About page logic

import { Component, OnInit } from '@angular/core';
import { DeveloperTemplateInterface, TesisInformationInterface } from '@interfaces';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  // Information of the tesis study
  introduction: string = 'En una sociedad, se debe buscar que los procesos electorales democráticos se realicen de manera segura, transparente e inequívoca. Actualmente, este tipo de procesos en la democracia colombiana, no brindan las garantías necesarias a los electores y elegidos, permitiendo que se generen alteraciones o errores en los resultados. Además, esta es una de las razones por las que existe un abstencionismo alto como el presentado en las elecciones presidenciales del año 2018 (46.6%). En los últimos años, el voto electrónico (e-voting) se ha presentado como una alternativa al fraude electoral; sin embargo, naciones con democracias consolidadas, argumentan que no lo utilizan principalmente por problemas en la seguridad.';
  objective: string = 'Identificar tácticas arquitectónicas de seguridad utilizadas en la construcción de software.';
  methodoly: string = 'Se utilizó como técnica la revisión sistemática de literatura (LRS) recurriendo a las fases: formular preguntas, planear y ejecutar la búsqueda, seleccionar estudios relevantes, evaluar la calidad de los estudios, extraer los datos y sintetizar los resultados.';
  results: string = 'Las tácticas arquitectónicas de seguridad, se orientan principalmente a detectar, resistir, reaccionar y recuperarse, a ataques; junto con auditar, autenticar y establecer sesiones seguras. Los campos de mayor aplicación para estas tácticas son la industria, la academia y el gobierno. Además, se logra sistematizar las tácticas, identificando las principales prácticas, efectos y los recursos utilizados para implementarlas.';
  conclusions: string = 'A partir de las tácticas de seguridad identificadas, se logra establecer que, las principales tácticas arquitectónicas de seguridad para e-voting se centran en detectar, resistir, reaccionar y recuperarse, a ataques.';

  // systemInfo is the 
  systemInfo: TesisInformationInterface[] = [
    {title:'Introducción', cont: this.introduction},
    {title:'Objetivo', cont: this.objective},
    {title:'Metodología', cont: this.methodoly},
    {title:'Resultados', cont: this.results},
    {title:'Conclusiones', cont: this.conclusions},
  ]

  // developerArray is the array with the developers system information
  developerArray: DeveloperTemplateInterface[] = [
    {
      name: 'Daniel',
      lastName: 'Burbano',
      email: 'danielburbano19@gmail.com',
      phoneNumber: '+57 3105927383',
      linkedIn: 'https://www.linkedin.com/in/daniel-esteban-burbano-salas-8b0a931a1/',
      facebook: 'https://www.facebook.com/danielesteban.burbano.79',
      twitter: 'https://twitter.com/danielburbano20',
      instagram: 'https://www.instagram.com/danielestebanburbano/',
      image: "assets/img/developer-daniel.jpg",
    },
    {
      name: 'Francisco',
      lastName: 'Zambrano',
      email: 'franciscozambrano@outlook.es',
      phoneNumber: '+57 3105927383',
      linkedIn: 'https://www.linkedin.com/in/zsfrancisco/',
      facebook: 'https://www.facebook.com/ZSFrancisco',
      twitter: 'https://twitter.com/ZSFrancisco',
      instagram: 'https://www.instagram.com/zsfrancisco/',
      image: "assets/img/developer-francisco.jpg",
    },
    {
      name: 'Giovanni',
      lastName: 'Hernandez',
      email: 'gihernandezp@gmail.com',
      phoneNumber: '+57 300 6542483',
      linkedIn: 'https://www.linkedin.com/in/giovanni-hernandez-949b6456/',
      facebook: 'https://www.facebook.com/giovanni.hernandezp',
      twitter: 'https://twitter.com/gihernandezp',
      instagram: 'https://www.instagram.com/hernandezpgiovanni/',
      image: "assets/img/adviser-giovanni.jpg",
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

  // getting developers
  get developers(): DeveloperTemplateInterface[] {
    return this.developerArray;
  }

}
