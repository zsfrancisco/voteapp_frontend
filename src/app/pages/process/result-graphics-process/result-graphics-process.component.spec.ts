import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultGraphicsProcessComponent } from './result-graphics-process.component';

describe('ResultGraphicsProcessComponent', () => {
  let component: ResultGraphicsProcessComponent;
  let fixture: ComponentFixture<ResultGraphicsProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultGraphicsProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultGraphicsProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
