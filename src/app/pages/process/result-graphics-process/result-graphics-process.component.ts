import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { ProcessService } from '../../../services/process.service';
import { SweetAlertService } from '../../../services/sweet-alert.service';

@Component({
  selector: 'app-result-graphics-process',
  templateUrl: './result-graphics-process.component.html',
  styleUrls: ['./result-graphics-process.component.css']
})
export class ResultGraphicsProcessComponent implements OnInit {

  currentProcessId: string = '';

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['Proceso de votación'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [];

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public randomize(): void {
    // Only Change 3 values
    const data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    this.barChartData[0].data = data;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
  ) { }

  processResults: any;

  isLoading: boolean = true;
  isError: boolean = false;

  totalVotes: string = '';
  candidatesNumber: string = '';
  votersNumber: string = '';
  activeVotersPorcentage: string = '';
  abstentionismVotersPorcentage: string = '';

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.currentProcessId = params['processId'];
    });
    this.loadResults();
  }

  async loadResults() {
    try {
      this.isLoading = true;
      const results: any = await this.processService.viewAdminWinner(this.currentProcessId);
      console.log('resultados: ', results);
      console.log("candidatos: ", Object.values(results.results_by_candidate));
      
      results.results_by_candidate.forEach((label: any) => {
        this.barChartData.push(
          { data: [label.votes_porcentage, 100], label: `${label.first_name} ${label.last_name}` }
        )
      });
      this.totalVotes = results.total_votes;
      this.candidatesNumber = results.candidates_number;
      this.votersNumber = results.voters_number;
      this.activeVotersPorcentage = results.active_voters_porcentage;
      this.abstentionismVotersPorcentage = results.abstentionism_voters_porcentage;
    } catch (error) {
      this.isError = true;  
      this.sweetAlertService.errorAlertRedirect(`No se lograron cargar los resultados del proceso con id ${this.currentProcessId}`, '/');
    }
    this.isLoading = false;
  }

}
