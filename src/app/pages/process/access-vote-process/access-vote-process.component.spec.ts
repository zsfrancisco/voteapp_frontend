import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessVoteProcessComponent } from './access-vote-process.component';

describe('AccessVoteProcessComponent', () => {
  let component: AccessVoteProcessComponent;
  let fixture: ComponentFixture<AccessVoteProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessVoteProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessVoteProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
