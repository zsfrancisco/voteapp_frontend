import { Component, OnInit } from '@angular/core';
import { ProcessCardTemplateInterface, ProcessReturned, PublicProcessResultInterface } from '@interfaces';
import { ProcessService } from 'src/app/services/process.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-result-process',
  templateUrl: './result-process.component.html',
  styleUrls: ['./result-process.component.css']
})
export class ResultProcessComponent implements OnInit {

  // Process
  processesArray: ProcessCardTemplateInterface[] = [];
  processesArrayWhenIsSearched: ProcessCardTemplateInterface[] = [];

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;
  isSearch: boolean = false;
  isSearchIsEmpty: boolean = false;

  constructor(
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.loadProcess()
  }

  // loadProcess fill the process array for showed in the template
  async loadProcess() {
    this.isLoading = true;
    this.isError = false;
    this.isEmpty = false;
    this.processesArray = [];
    try {
      let processprov: ProcessReturned[] = await this.processService.getProcesses() as ProcessReturned[];
      for (let index = 0; index < processprov.length; index++) {
        if (processprov[index].status == '2') {
          console.log('Entra al for normal');
          const processImage = await this.processService.getProcessImage(processprov[index].id);
          const proccessCard: ProcessCardTemplateInterface = {
            id: processprov[index].id,
            name: processprov[index].name,
            description: processprov[index].description,
            start_date: processprov[index].start_date,
            image: processImage,
            status: processprov[index].status,
          }
          this.processesArray.push(proccessCard);
        }
      }
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlert('Ocurrió un error cargando los registros, inténtelo más tarde');
    }
    if (this.processesArray.length > 0) {
      this.isEmpty = false;
    } else this.isEmpty = true;
    console.log('Procesos retornados: ', this.processes);
    this.isLoading = false
    // console.log(this.processes);
  }

  // processes returns the data with the pagination
  get processes(): ProcessCardTemplateInterface[] {
    return this.processesArray;
  }

  // searchProcess searchs a processes by name
  searchProcess(text: string) {
    console.log(`El valor del texto: ${text}`);
    text = text.toLowerCase();
    if (text === '') {
      this.isSearch = false;
    } else {
      this.processesArrayWhenIsSearched = [];
      this.isSearch = true;
      this.processes.forEach(process => {
        let processName = process.name.toLowerCase();
        if (processName.indexOf(text) >= 0) this.processesArrayWhenIsSearched.push(process);
      });
      this.processesArrayWhenIsSearched.length <= 0 ? this.isSearchIsEmpty = true : this.isSearchIsEmpty = false;
    }
  }

  async viewWinner(processId: string) {
    this.router.navigateByUrl(`process/results/${processId}`);
    // try {
    //   this.sweetAlertService.loadingAlert('Obteniendo ganador');
    //   const results = await this.processService.viewPublicWinner(processId) as PublicProcessResultInterface[];
    //   console.log('Resultados desde componente: ', results);
    //   let description: string = '';
    //   results.forEach(result => {
    //     const name =  `${result.first_name} ${result.last_name}`;
    //     const count = result.votes_count;
    //     description = description + `<div class="container text-left">
    //     <span class="font-05668D font-weight-bold">Nombre:</span> <span>${name}</span> <br>
    //     <span class="font-05668D font-weight-bold">Votos:</span> <span>${count}</span> <br>
    //   </div>`;
    //   });

    //   Swal.fire({
    //     title: '<span class="font-028090">Detalles del Ganador<span/>',
    //     html: `${description}`,
    //     confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
    //     confirmButtonColor: '#05668d',
    //   });
    // } catch (error) {
    //   this.sweetAlertService.errorAlert('No se pudo obtener el resultado del ganador, intente más tarde');
    // }
  }

}
