import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SweetAlertService } from '../../../services/sweet-alert.service';

@Component({
  selector: 'app-error-access',
  templateUrl: './error-access.component.html',
  styleUrls: ['./error-access.component.css']
})
export class ErrorAccessComponent implements OnInit {

  message = '';
  errorId = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private sweetAlertService: SweetAlertService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => this.errorId = params['errorId'] );
    switch (this.errorId) {
      case '1':
        this.message = 'Identificación del proceso de votación no fue encontrado';
        break;
      case '2':
        this.message = 'No se pudo reconocer al votante';
        break;
      case '3':
        this.message = 'Error al autenticar al votante por favor intentar nuevamente';
        break;
      default:
        this.message = 'Error no existe';
        break;
    }
    this.sweetAlertService.errorAlertRedirect(this.message, '/');
  }

}
