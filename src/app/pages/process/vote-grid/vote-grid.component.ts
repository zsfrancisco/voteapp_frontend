import { Component, OnInit } from '@angular/core';
import { CandidateVoteDetailsTemplateInterface, ProcessReturned, CandidateReturnedInterface, RegisterVotePetitionInterface, RegisterVoteInterface, VoterGetToken } from '@interfaces';
import { CandidateService } from 'src/app/services/candidate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProcessService } from 'src/app/services/process.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import Swal from 'sweetalert2';
import { VoterService } from '../../../services/voter.service';
import { AuthService } from '../../../services/auth.service';
import { IpAddressService } from '../../../services/ip-address.service';

@Component({
  selector: 'app-vote-grid',
  templateUrl: './vote-grid.component.html',
  styleUrls: ['./vote-grid.component.css']
})
export class VoteGridComponent implements OnInit {

  private candidatesArray: CandidateVoteDetailsTemplateInterface[] = [];
  private processId: string = '';
  processName: string;

  private voterId: string = '';
  private voterName: string = '';
  private voterToken: string = '';
  
  private ipAddress: string = '';

  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;


  get candidates() {
    return this.candidatesArray;
  }

  constructor(
    private candidateService: CandidateService,
    private activatedRoute: ActivatedRoute,
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    private voterService: VoterService,
    private authService: AuthService,
    private ipAddressService: IpAddressService,
  ) { }

  ngOnInit(): void {
    this.getIpAddress();
    this.activatedRoute.params.subscribe(params => {
      this.processId = params['processId'];
      this.voterId = params['voterId'];
    });
    this.loadCandidates();
  }

  // Get the ip address of the current device
  getIpAddress() {
    this.ipAddressService.getIPAddress().subscribe((res: any) => {
      this.ipAddress = res.ip;
      this.getToken();
    });
  }

  // loadCandidates gets the information of the candidates
  async loadCandidates() {
    this.isLoading = true;
    this.isEmpty = false;
    this.isError = false;
    this.candidatesArray = [];
    try {
      // getting process information
      const processReturned = await this.processService.getProcess(this.processId);
      // console.log('Proceso retornado: ', processReturned);
      this.processName = (processReturned as ProcessReturned).name;

      // Getting candidates records
      const candidates: CandidateReturnedInterface[] = await this.candidateService.getCandidatesByProcessId(this.processId) as CandidateReturnedInterface[];
      for (let index = 0; index < candidates.length; index++) {
        // console.log('Entra al for normal');
        const candidateImage = candidates[index].identification != '-' ? await this.candidateService.getCandidateImage(this.processId, candidates[index].identification) : '';
        const candidateCard: CandidateVoteDetailsTemplateInterface = {
          id: candidates[index].id,
          identification: candidates[index].identification,
          first_name: candidates[index].first_name,
          last_name: candidates[index].last_name,
          birthdate: candidates[index].birthdate,
          gender: candidates[index].gender,
          email: candidates[index].email,
          description: candidates[index].description,
          pivot: candidates[index].pivot,
          proposals: candidates[index].proposals,
          image: candidateImage,
        }
        this.candidatesArray.push(candidateCard);
        this.candidatesArray.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
      }
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect(`Error al cargar los candidatos del proceso ${this.processId}`, '/process/panel');
    }
    this.isLoading = false;
    console.log('Arregloo de candidatos: ', this.candidatesArray);
  }


  // RegisterVote saves the vote for the candidate and checks the voter 
  RegisterVote(candidate: CandidateVoteDetailsTemplateInterface) {
    Swal.fire({
      title: `¿Está seguro de votar por ${candidate.first_name} ${candidate.last_name}?`,
      text: 'Este proceso no se puede revertir',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#05668d',
      cancelButtonColor: '#d33',
      confirmButtonText: `Si, Votar`,
      cancelButtonText: 'Seleccionar otro',
      reverseButtons: true,
    }).then(async (result) => {
      if (result.value) {
        this.sweetAlertService.loadingAlert(`${this.voterName}, se está registrando voto a ${candidate.first_name} ${candidate.last_name}`);
        try {
          const registerVotePetition: RegisterVoteInterface = {
            voter_id: this.voterId,
            candidate_id: candidate.id,
            voting_process_id: this.processId,
            ip_address: this.ipAddress,
          }
          const registerVoterPetition: RegisterVotePetitionInterface = {
            voting_process_id: this.processId,
            voter_id: this.voterId,
          }
          await this.processService.registerVote(registerVotePetition, this.voterToken);
          await this.voterService.registerVoter(registerVoterPetition);

          this.sweetAlertService.succesAlert(`Su voto por ${candidate.first_name} ${candidate.last_name} se ha registrado exitosamente`);
          setTimeout(() => {
            this.authService.closeVoterSession();
            this.router.navigateByUrl('/');
          }, 1500);
        } catch (error) {
          console.log('error: ', error);
          this.sweetAlertService.errorAlert(`Su voto por ${candidate.first_name} ${candidate.last_name} no se registró, inténtelo más tarde`);
        }
      }
    });
  }

  async getToken() {
    try {
      const petition: VoterGetToken = {
        voting_process_id: this.processId,
        voter_identification: this.voterId,
        ip_address: this.ipAddress,
      }
      console.log('Petición: ', petition);
      const answer: any = await this.voterService.getToken(petition);
      this.voterToken = answer.token;
      this.voterName = `${answer.voter_first_name} ${answer.voter_last_name}`;
      this.voterId = answer.voter_id;
    } catch (error) {
      this.isError = true;
      this.isLoading = false;
      console.log('Entra al catch: ', error);
      this.sweetAlertService.errorAlertRedirect(error, `/`);
    }
  }


}
