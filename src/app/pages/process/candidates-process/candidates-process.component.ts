import { Component, OnInit } from '@angular/core';
import { CandidateService } from '../../../services/candidate.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidateReturnedInterface, ProcessReturned, CandidateVoteDetailsTemplateInterface } from '@interfaces';
import { ProcessService } from '../../../services/process.service';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-candidates-process',
  templateUrl: './candidates-process.component.html',
  styleUrls: ['./candidates-process.component.css']
})
export class CandidatesProcessComponent implements OnInit {

  private candidatesArray: CandidateVoteDetailsTemplateInterface[] = [];
  private processId: string = '';
  processName: string;
  processStatus: string;

  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;

  get candidates() {
    return this.candidatesArray;
  }

  constructor(
    private candidateService: CandidateService,
    private activatedRoute: ActivatedRoute,
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => this.processId = params['processId']);
    this.loadCandidates();
  }

  // loadCandidates gets the information of the candidates
  async loadCandidates() {
    this.isLoading = true;
    this.isEmpty = false;
    this.isError = false;
    this.candidatesArray = [];
    try {
      // getting process information
      const processReturned = await this.processService.getProcess(this.processId);
      // console.log('Proceso retornado: ', processReturned);
      this.processName = (processReturned as ProcessReturned).name;
      this.processStatus = (processReturned as ProcessReturned).status;

      // Getting candidates records
      const candidates: CandidateReturnedInterface[] = await this.candidateService.getCandidatesByProcessId(this.processId) as CandidateReturnedInterface[];
      for (let index = 0; index < candidates.length; index++) {
        // console.log('Entra al for normal');
        const candidateImage = candidates[index].identification != '-' ? await this.candidateService.getCandidateImage(this.processId, candidates[index].identification) : '';
        const candidateCard: CandidateVoteDetailsTemplateInterface = {
          id: candidates[index].id,
          identification: candidates[index].identification,
          first_name: candidates[index].first_name,
          last_name: candidates[index].last_name,
          birthdate: candidates[index].birthdate,
          gender: candidates[index].gender,
          email: candidates[index].email,
          description: candidates[index].description,
          pivot: candidates[index].pivot,
          proposals: candidates[index].proposals,
          image: candidateImage,
        }
        this.candidatesArray.push(candidateCard);
        this.candidatesArray.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
      }
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect(`Error al cargar los candidatos del proceso ${this.processId}`, '/process/panel');
    }
    this.isLoading = false;
    console.log('Arregloo de candidatos: ', this.candidatesArray);
  }

  // View details of a specific candidate of the current process
  viewDetails(candidate: CandidateReturnedInterface) {
    const candidateId = candidate.id;
    this.router.navigateByUrl(`process/${this.processId}/candidates-panel/${candidateId}/proposals`);
  }

  // goAccess redirects to the access to vote
  goAccess() {
    console.log('Candidates-process este es el id del proceso: ', this.processId);
    // this.router.navigateByUrl(`/process/${this.processId}/access/vote`);
    document.location.href = `http://localhost/evoting/public/authentication/${this.processId}`;
    // this.router.navigateByUrl(`http://localhost/evoting/public/authentication/${this.processId}`);
  }
}
