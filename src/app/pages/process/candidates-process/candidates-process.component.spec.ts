import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatesProcessComponent } from './candidates-process.component';

describe('CandidatesProcessComponent', () => {
  let component: CandidatesProcessComponent;
  let fixture: ComponentFixture<CandidatesProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatesProcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatesProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
