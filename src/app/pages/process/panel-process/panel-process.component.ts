// This component controls the logic to get the processes information

import { Component, OnInit } from '@angular/core';
import { ProcessService } from 'src/app/services/process.service';
import { ProcessReturned, ChangeStatusProcessInterface } from '@interfaces';
import Swal from 'sweetalert2';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-panel-process',
  templateUrl: './panel-process.component.html',
  styleUrls: ['./panel-process.component.css']
})
export class PanelProcessComponent implements OnInit {

  // Array with electoral processes
  processesArray: ProcessReturned[] = [];

  // controls the pagination
  page = 1;
  pageSize = 4;
  collectionSize = this.processesArray.length;

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;

  constructor(
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    private authService: AuthService,
  ) { }


  ngOnInit() {
    this.loadProcess();
  }

  // loadProcess loads the data
  async loadProcess() {
    this.processesArray = [];
    try {
      let processprov = await this.processService.getProcesses();
      this.processesArray = processprov as ProcessReturned[];
      this.collectionSize = this.processesArray.length;
      this.processesArray.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
    } catch (error) {
      this.isError = true;
    }
    this.isLoading = false;
    // console.log(this.processes);
  }

  // processes returns the data with the pagination
  get processes(): any[] {
    return this.processesArray
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  alertAction(
    statusId: string,
    title: string,
    confirmButtonText: string,
    processId: string,
    loadingText: string,
    successText: string,
    errorText: string,
    confirmButtonColor: string = '#05668d',
    cancelButtonColor: string = '#d33',
  ) {
    Swal.fire({
      title: title,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: confirmButtonColor,
      cancelButtonColor: cancelButtonColor,
      confirmButtonText: confirmButtonText,
      cancelButtonText: 'Descartar cambios',
      reverseButtons: true,
    }).then(async (result) => {
      if (result.value) {
        this.sweetAlertService.loadingAlert(loadingText);
        try {
          // Doing the petition in service
          const petition: ChangeStatusProcessInterface = {
            user_id: this.authService.readUserId(),
            voting_process_id: processId,
            status: statusId,
          }
          await this.processService.changeStatus(petition);
          this.sweetAlertService.succesAlert(successText);
          this.loadProcess();
        } catch (error) {
          console.log('error: ', error);
          this.sweetAlertService.errorAlert(errorText);
        }
      }
    });
  }

  editProcess(processId: string) {
    this.router.navigateByUrl(`process/edit_process/${processId}`);
  }

  // startProcess change the process status to 1
  startProcess(processId: string) {
    this.alertAction(
      '1',
      '¿Está seguro de iniciar el proceso de votación?',
      'Si, iniciar',
      processId,
      'Suspendiendo proceso electoral',
      `El proceso electoral ${processId} se inicio satisfactoriamente`,
      `El proceso electoral ${processId} no se inició, inténtelo nuevamente más tarde`,
    );
  }

  // startProcess change the process status to 2
  finishProcess(processId: string) {
    this.alertAction(
      '2',
      '¿Está seguro de terminar el proceso de votación?',
      'Si, terminar',
      processId,
      'Suspendiendo proceso electoral',
      `El proceso electoral ${processId} finalizó satisfactoriamente`,
      `El proceso electoral ${processId} no finalizó, inténtelo nuevamente más tarde`,
    );
  }

  // cancelProcess cancels a specific electoral process
  cancelProcess(processId: string) {
    this.alertAction(
      '3',
      '¿Está seguro de cancelar el proceso de votación?',
      'si, cancelar proceso',
      processId,
      'Cancelando proceso',
      `El proceso ${processId} se canceló satisfactoriamente`,
      `El proceso ${processId} no se canceló, inténtelo más tarde`,
      '#d33',
      '#05668d',
    );
  }
  // pauseProcess  change the process status to 3
  pauseProcess(processId: string) {
    this.alertAction(
      '4',
      '¿Está seguro de suspender el proceso de votación?',
      'Si, suspender',
      processId,
      'Suspendiendo proceso electoral',
      `El proceso electoral ${processId} se suspendió satisfactoriamente`,
      `El proceso electoral ${processId} no se suspedió, inténtelo nuevamente más tarde`,
    );
  }

  // addCandidate redirects to the form to create a candidate
  addCandidate(processId: string) {
    this.router.navigateByUrl(`process/${processId}/candidates/new_candidate`);
  }

  // viewCandidates redirect to the template with candidates data of a specific electoral process
  viewCandidates(processId: string) {
    this.router.navigateByUrl(`process/${processId}/candidates-panel`);
  }

  // viewDetails shows details of a specific process
  async viewDetails(process: ProcessReturned) {
    console.log('Viendo detalles del proceso: ', process);
    let processStatus: string;
    switch (process.status) {
      case '0':
        processStatus = 'Creado';
        break;
      case '1':
        processStatus = 'Iniciado';
        break;
      case '2':
        processStatus = 'Finalizado';
        break;
      case '3':
        processStatus = 'Cancelado';
        break;
      case '4':
        processStatus = 'Suspendido';
        break;

      default:
        processStatus = 'Creado';
        break;
    }
    const voters = await this.processService.getVotersByProcess(process.id);
    Swal.fire({
      title: '<span class="font-028090">Detalles del proceso electoral<span/>',
      html:
        `
        <div class="container text-left">
          <span class="font-05668D font-weight-bold">Nombre:</span> <span>${process.name}</span> <br>
          <span class="font-05668D font-weight-bold">Descripción:</span> <span>${process.description}</span> <br>
          <span class="font-05668D font-weight-bold">Fecha inicio:</span> <span>${process.start_date}</span> <br>
          <span class="font-05668D font-weight-bold">Fecha fin:</span> <span>${process.end_date}</span> <br>
          <span class="font-05668D font-weight-bold">Número de candidatos:</span> <span>${process.candidates_number}</span> <br>
          <span class="font-05668D font-weight-bold">Estado:</span> <span>${processStatus}</span> <br>
          <span class="font-05668D font-weight-bold">Votantes:</span> <a href="${voters}" target="_blank">Descargar reporte</a> <br>
        </div>
        `,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
      confirmButtonColor: '#05668d',
    });
  }

  async viewWinner(processId: string) {
    try {
      this.sweetAlertService.loadingAlert('Obteniendo ganador');
      const results: any = await this.processService.viewAdminWinner(processId);
      console.log('Resultados desde componente: ', results);
      let description: string = '';
      results.results_by_candidate.forEach((result: any) => {
        const name = `${result.first_name} ${result.last_name}`;
        const count = result.votes_count;
        description = description + `<div class="container text-left">
        <span class="font-05668D font-weight-bold">Nombre:</span> <span>${name}</span> <br>
        <span class="font-05668D font-weight-bold">Votos:</span> <span>${count}</span> <br>
      </div>`;
      });
      Swal.fire({
        title: '<span class="font-028090">Detalles del Ganador<span/>',
        html: `${description} <a href="${results.url_report}" target="_blank">Descargar reporte</a>`,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar',
        confirmButtonColor: '#05668d',
      });
    } catch (error) {
      this.sweetAlertService.errorAlert('No se pudo obtener el resultado del ganador, intente más tarde');
    }
  }

  // viewCandidates redirect to the template with candidates data of a specific electoral process
  authLogs(processId: string) {
    this.router.navigateByUrl(`process/${ processId }/auth_logs`);
  }

}
