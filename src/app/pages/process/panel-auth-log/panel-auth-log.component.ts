import { Component, OnInit } from '@angular/core';
import { IAuthenticationLog } from '@interfaces';
import { ProcessService } from 'src/app/services/process.service';
import { SweetAlertService } from 'src/app/services/sweet-alert.service';
import { ActivatedRoute } from '@angular/router';
import { DateService } from '../../../services/date.service';
import moment from 'moment';

@Component({
  selector: 'app-panel-auth-log',
  templateUrl: './panel-auth-log.component.html',
  styleUrls: ['./panel-auth-log.component.css']
})
export class PanelAuthLogComponent implements OnInit {

  // _authLogsArray array with the authentications logs information
  private _authLogsArray: IAuthenticationLog[] = [];

  // controls the pagination
  page = 1;
  pageSize = 4;
  collectionSize = this._authLogsArray.length;

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;

  bannerText: string = "";

  constructor(
    private processService: ProcessService,
    private sweetAlertService: SweetAlertService,
    private activatedRoute: ActivatedRoute,
    private dateService: DateService,
  ) { }

  ngOnInit(): void {
    let processId: string;
    this.activatedRoute.params.subscribe( params => processId = params["processId"] );
    this.bannerText = `LOG RECONOCIMIENTO FACIAL - PROCESO ${ processId }`;
    this.getAuthLogs(processId);
  }

  async getAuthLogs(processId) {
    try {
      this.isLoading = true;
      this._authLogsArray = [];
      this.isEmpty = true;
      this.isError = false;
      this._authLogsArray = await this.processService.authenticationLogs(processId) as IAuthenticationLog[];
      // console.log("Auth logs: ", this._authLogsArray);
      this.collectionSize = this._authLogsArray.length;
      this._authLogsArray.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect(error.error.message, "process/panel");
    }
    this.isLoading = false;
  }
  
  public get authLogs() : IAuthenticationLog[] {
    return this._authLogsArray;
  }
  
  date(date: string) {
    const dateProv = new Date(date);
    return moment(dateProv, "MM-DD-YYYY").format("MM-DD-YYYY HH:mm:ss");
  }

}
