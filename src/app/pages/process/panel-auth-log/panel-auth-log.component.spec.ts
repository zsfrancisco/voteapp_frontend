import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelAuthLogComponent } from './panel-auth-log.component';

describe('PanelAuthLogComponent', () => {
  let component: PanelAuthLogComponent;
  let fixture: ComponentFixture<PanelAuthLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelAuthLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelAuthLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
