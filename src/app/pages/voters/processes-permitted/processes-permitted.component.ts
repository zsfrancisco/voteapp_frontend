import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { ProcessSuitableForUserInterface } from '@interfaces';
import { ProcessService } from '../../../services/process.service';

@Component({
  selector: 'app-processes-permitted',
  templateUrl: './processes-permitted.component.html',
  styleUrls: ['./processes-permitted.component.css']
})
export class ProcessesPermittedComponent implements OnInit {

  voterId: string = '';

  processesArray: ProcessSuitableForUserInterface[] = [];

  // controls the pagination
  page = 1;
  pageSize = 4;
  collectionSize = this.processesArray.length;

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private sweetAlertService: SweetAlertService,
    private processService: ProcessService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.voterId = params['voterId'];
    });
    this.loadProcesses();
  }

  // loadProcesses gets the data with the processes permitted for the user
  async loadProcesses() {
    this.processesArray = [];
    this.isLoading = true;
    this.isError = false;
    this.isEmpty = false;
    try {
      let processprov = await this.processService.consultSuitable(this.voterId);
      this.processesArray = processprov as ProcessSuitableForUserInterface[];
      this.collectionSize = this.processesArray.length;
      this.processesArray.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect(`El ciudadano con identificación ${this.voterId} no es apto para votar en ningú proceso electoral`, '/home');
    }
    this.isLoading = false;
    console.log('Procesos cargados: ', this.processes);
    
  }

  get processes() {
    return this.processesArray
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

}
