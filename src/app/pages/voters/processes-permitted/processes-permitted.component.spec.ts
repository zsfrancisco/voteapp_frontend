import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessesPermittedComponent } from './processes-permitted.component';

describe('ProcessesPermittedComponent', () => {
  let component: ProcessesPermittedComponent;
  let fixture: ComponentFixture<ProcessesPermittedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessesPermittedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessesPermittedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
