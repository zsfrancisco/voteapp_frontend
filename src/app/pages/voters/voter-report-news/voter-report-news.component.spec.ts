import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoterReportNewsComponent } from './voter-report-news.component';

describe('VoterReportNewsComponent', () => {
  let component: VoterReportNewsComponent;
  let fixture: ComponentFixture<VoterReportNewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoterReportNewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoterReportNewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
