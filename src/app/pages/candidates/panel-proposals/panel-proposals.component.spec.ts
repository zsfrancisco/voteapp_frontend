import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelProposalsComponent } from './panel-proposals.component';

describe('PanelProposalsComponent', () => {
  let component: PanelProposalsComponent;
  let fixture: ComponentFixture<PanelProposalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelProposalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelProposalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
