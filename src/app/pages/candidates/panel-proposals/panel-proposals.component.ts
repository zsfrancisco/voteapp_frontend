import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidateService } from '../../../services/candidate.service';
import { CandidateReturnedInterface, CandidateVoteDetailsTemplateInterface } from '@interfaces';
import { SweetAlertService } from '../../../services/sweet-alert.service';

@Component({
  selector: 'app-panel-proposals',
  templateUrl: './panel-proposals.component.html',
  styleUrls: ['./panel-proposals.component.css']
})
export class PanelProposalsComponent implements OnInit {

  private _processId: string;
  private _candidateId: string;
  private _candidateTemplate: CandidateVoteDetailsTemplateInterface;

  isLoading: boolean = true;
  isError: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private candidateService: CandidateService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this._processId = params['processId'];
      this._candidateId = params['candidateId'];
    });
    this.getCandidate();
  }

  // getCandidate gets the information of the current candidate and fills the candidateTemplate's interface
  async getCandidate() {
    try {
      this.isLoading = true;
      this.isError = false;
      const answer: any = await this.candidateService.getCandidateById(this._candidateId)
      const candidate = answer as CandidateReturnedInterface;
      // console.log('Candidato: ', candidate);
      const identification = candidate.identification;
      // console.log('Identificación: ', identification);
      const candidateImage = await this.candidateService.getCandidateImage(this._processId, identification);
      const candidateTemplate: CandidateVoteDetailsTemplateInterface = {
        id: candidate.id,
        identification: candidate.identification,
        first_name: candidate.first_name,
        last_name: candidate.last_name,
        birthdate: candidate.birthdate,
        gender: candidate.gender,
        email: candidate.email,
        description: candidate.description,
        pivot: candidate.pivot,
        proposals: candidate.proposals,
        image: candidateImage,
      }
      this._candidateTemplate = candidateTemplate;
    } catch (error) {
      console.log("Error: ", error);
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect('Ocurrió error al cargar la información del candidato', `/`);
    }
    this.isLoading = false;
  }

  public get candidate(): CandidateVoteDetailsTemplateInterface {
    return this._candidateTemplate;
  }

  // onBack redirects to the view of the candidates
  onBack() {
    this.router.navigateByUrl(`/process/${ this._processId }/candidates`);
  }

}
