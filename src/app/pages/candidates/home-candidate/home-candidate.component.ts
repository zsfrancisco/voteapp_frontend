import { Component, OnInit } from '@angular/core';
import { CandidateHomeInformation } from '@interfaces';
import { AuthService } from '../../../services/auth.service';
import { CandidateService } from '../../../services/candidate.service';

@Component({
  selector: 'app-home-candidate',
  templateUrl: './home-candidate.component.html',
  styleUrls: ['./home-candidate.component.css']
})
export class HomeCandidateComponent implements OnInit {

  candidateId: string = "";
  votingProcessId: string = "";
  candidateInformation: CandidateHomeInformation;

  private _isLoading = true;
  private _isError = false;

  candidateImage: string = "";

  constructor(
    private authService: AuthService,
    private candidateService: CandidateService,
  ) { }

  ngOnInit(): void {
    this.getUserInfo();
  }

  async getUserInfo() {
    try {
      this._isLoading = true;
      this._isError = false;
      const userId = this.authService.readUserId();
      const candidate: any = await this.authService.getUserById(userId);
      console.log("Información del candidato: ", candidate);
      this.candidateInformation = {
        id: candidate.id,
        identification: candidate.identification,
        first_name: candidate.first_name,
        last_name: candidate.last_name,
        birthdate: candidate.birthdate,
        gender: candidate.gender,
        email: candidate.email,
      }
      this.candidateId = candidate.custom_data.candidate_id;
      this.votingProcessId = candidate.custom_data.voting_process_id;
      this.candidateImage = await this.candidateService.getCandidateImage(this.votingProcessId, this.candidateInformation.identification) as string;
    } catch (error) {
      this._isError = true;
    }
    this._isLoading = false;
  }

  public get isLoading(): boolean {
    return this._isLoading;
  }

  public get isError(): boolean {
    return this._isError;
  }

}
