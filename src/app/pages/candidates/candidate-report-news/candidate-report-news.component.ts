import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-candidate-report-news',
  templateUrl: './candidate-report-news.component.html',
  styleUrls: ['./candidate-report-news.component.css']
})
export class CandidateReportNewsComponent implements OnInit {

  candidateId: string = "";
  votingProcessId: string = "";

  private _isLoading = true;
  private _isError = false;

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.getUserInfo();
  }

  async getUserInfo() {
    try {
    this._isLoading = true;
    this._isError = false;
    const userId = this.authService.readUserId();
      const candidate: any = await this.authService.getUserById(userId);
      console.log("Información del candidato: ", candidate);
      this.candidateId = candidate.custom_data.candidate_id;
      this.votingProcessId = candidate.custom_data.voting_process_id;
    } catch (error) {
      this._isError = true;
    }
    this._isLoading = false;
  }

  public get isLoading(): boolean {
    return this._isLoading;
  }

  public get isError(): boolean {
    return this._isError;
  }

}
