// This component manages the logic of candidates panel

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidateReturnedInterface, ProcessReturned } from '@interfaces';
import { CandidateService } from '../../../services/candidate.service';
import { SweetAlertService } from '../../../services/sweet-alert.service';
import { ProcessService } from '../../../services/process.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-panel-candidates',
  templateUrl: './panel-candidates.component.html',
  styleUrls: ['./panel-candidates.component.css']
})
export class PanelCandidatesComponent implements OnInit {

  // features of the current process
  processId: string;
  processState: string;
  processName: string;

  // Array with candidates records
  candidatesArray: CandidateReturnedInterface[] = [];

  // Flags
  isLoading: boolean = true;
  isError: boolean = false;
  isEmpty: boolean = false;

  // Button text for the banner component
  buttonBannerText: string;

  // Controls to table
  page = 1;
  pageSize = 4;
  collectionSize = this.candidatesArray.length;

  reportCandidates: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private candidateService: CandidateService,
    private sweetAlertService: SweetAlertService,
    private processService: ProcessService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    // Getting the processId of the route
    this.activatedRoute.params.subscribe(params => {
      this.processId = params['processId'];
      this.buttonBannerText = `CANDIDATOS DEL PROCESO ${this.processId}`;
    });
    this.loadCandidates();
  }

  // Getting all of the records of candidates in data base
  async loadCandidates() {
    this.isLoading = true;
    this.isEmpty = false;
    this.isError = false;
    this.candidatesArray = [];
    try {
      // getting process information
      const processReturned = await this.processService.getProcess(this.processId);
      // console.log('Proceso retornado: ', processReturned);
      this.processState = (processReturned as ProcessReturned).status;
      this.processName = (processReturned as ProcessReturned).name;

      // Getting candidates records
      const candidates = await this.candidateService.getCandidatesByProcessId(this.processId);
      this.reportCandidates = await this.candidateService.getReportCandidatesByProcessId(this.processId) as string;
      
      (candidates as CandidateReturnedInterface[]).forEach(candidate => {
        this.candidatesArray.push(candidate);
      });
      // Controls the table pagination
      this.candidatesArray.length <= 0 ? this.isEmpty = true : this.isEmpty = false;
      this.collectionSize = this.candidatesArray.length;
    } catch (error) {
      this.isError = true;
      this.sweetAlertService.errorAlertRedirect(`Error al cargar los candidatos del proceso ${this.processId}`, '/process/panel');
    }
    this.isLoading = false;
    console.log('Arregloo de candidatos: ', this.candidatesArray);
  }

  get candidates() {
    return this.candidatesArray
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  // View details of a specific candidate of the current process
  viewDetails(candidate: CandidateReturnedInterface) {
    const candidateId = candidate.id;
    this.router.navigateByUrl(`process/${this.processId}/candidates-panel/${candidateId}/proposals`);
  }

  // editCandidate edits a specific candidate of the current process
  editCandidate(candidateId: string) {
    this.router.navigateByUrl(`process/${this.processId}/candidates/edit_candidate/${candidateId}`);
  }

  // Delete a specific candidate of the current process
  deleteCandidate(candidateId: string) {
    Swal.fire({
      title: '¿Está seguro de borrar el candidato del proceso de votación?',
      text: 'Este proceso no se puede revertir',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#05668d',
      confirmButtonText: 'Si, borrar candidato',
      cancelButtonText: 'Descartar cambios',
      reverseButtons: true,
    }).then(async (result) => {
      if (result.value) {
        this.sweetAlertService.loadingAlert('Borrando caandidato');
        try {
          await this.candidateService.deleteCandidate(candidateId);
          this.sweetAlertService.succesAlert(`El candidato ${candidateId} se ha borrado exitosamente`);
          setTimeout(() => {
            this.loadCandidates();
          }, 1500);
        } catch (error) {
          console.log('error: ', error);
          this.sweetAlertService.errorAlert(`El candidato ${candidateId} no se borró, inténtelo más tarde`);
        }
      }
    });
  }

}
