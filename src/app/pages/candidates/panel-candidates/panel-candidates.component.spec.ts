import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelCandidatesComponent } from './panel-candidates.component';

describe('PanelCandidatesComponent', () => {
  let component: PanelCandidatesComponent;
  let fixture: ComponentFixture<PanelCandidatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelCandidatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelCandidatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
